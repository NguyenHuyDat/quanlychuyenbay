USE [DoAnLapTrinhUngDungQuanLy]
GO
/****** Object:  ForeignKey [FK_ChuyenBay_SanBay1]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChuyenBay_SanBay1]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChuyenBay]'))
ALTER TABLE [dbo].[ChuyenBay] DROP CONSTRAINT [FK_ChuyenBay_SanBay1]
GO
/****** Object:  ForeignKey [FK_ChuyenBay_SanBay2]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChuyenBay_SanBay2]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChuyenBay]'))
ALTER TABLE [dbo].[ChuyenBay] DROP CONSTRAINT [FK_ChuyenBay_SanBay2]
GO
/****** Object:  ForeignKey [FK_DSGSB_SanBay]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSGSB_SanBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachGiaSanBay]'))
ALTER TABLE [dbo].[DanhSachGiaSanBay] DROP CONSTRAINT [FK_DSGSB_SanBay]
GO
/****** Object:  ForeignKey [FK_DSSBTG_HangVe]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_HangVe]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachGiaSanBay]'))
ALTER TABLE [dbo].[DanhSachGiaSanBay] DROP CONSTRAINT [FK_DSSBTG_HangVe]
GO
/****** Object:  ForeignKey [FK_DSSBTG_ChuyenBay]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachSanBayTrungGian]'))
ALTER TABLE [dbo].[DanhSachSanBayTrungGian] DROP CONSTRAINT [FK_DSSBTG_ChuyenBay]
GO
/****** Object:  ForeignKey [FK_DSSBTG_SanBay]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_SanBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachSanBayTrungGian]'))
ALTER TABLE [dbo].[DanhSachSanBayTrungGian] DROP CONSTRAINT [FK_DSSBTG_SanBay]
GO
/****** Object:  ForeignKey [FK_PhieuDat_ChuyenBay]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuDat_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuDat]'))
ALTER TABLE [dbo].[PhieuDat] DROP CONSTRAINT [FK_PhieuDat_ChuyenBay]
GO
/****** Object:  ForeignKey [FK_PhieuDat_HanhKhach]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuDat_HanhKhach]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuDat]'))
ALTER TABLE [dbo].[PhieuDat] DROP CONSTRAINT [FK_PhieuDat_HanhKhach]
GO
/****** Object:  ForeignKey [FK_VeChuyenBay_ChuyenBay]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay] DROP CONSTRAINT [FK_VeChuyenBay_ChuyenBay]
GO
/****** Object:  ForeignKey [FK_VeChuyenBay_HangVe]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_HangVe]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay] DROP CONSTRAINT [FK_VeChuyenBay_HangVe]
GO
/****** Object:  ForeignKey [FK_VeChuyenBay_HanhKhach]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_HanhKhach]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay] DROP CONSTRAINT [FK_VeChuyenBay_HanhKhach]
GO
/****** Object:  Table [dbo].[DanhSachSanBayTrungGian]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachSanBayTrungGian]'))
ALTER TABLE [dbo].[DanhSachSanBayTrungGian] DROP CONSTRAINT [FK_DSSBTG_ChuyenBay]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_SanBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachSanBayTrungGian]'))
ALTER TABLE [dbo].[DanhSachSanBayTrungGian] DROP CONSTRAINT [FK_DSSBTG_SanBay]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DanhSachSanBayTrungGian]') AND type in (N'U'))
DROP TABLE [dbo].[DanhSachSanBayTrungGian]
GO
/****** Object:  Table [dbo].[PhieuDat]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuDat_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuDat]'))
ALTER TABLE [dbo].[PhieuDat] DROP CONSTRAINT [FK_PhieuDat_ChuyenBay]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuDat_HanhKhach]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuDat]'))
ALTER TABLE [dbo].[PhieuDat] DROP CONSTRAINT [FK_PhieuDat_HanhKhach]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PhieuDat]') AND type in (N'U'))
DROP TABLE [dbo].[PhieuDat]
GO
/****** Object:  Table [dbo].[VeChuyenBay]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay] DROP CONSTRAINT [FK_VeChuyenBay_ChuyenBay]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_HangVe]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay] DROP CONSTRAINT [FK_VeChuyenBay_HangVe]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_HanhKhach]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay] DROP CONSTRAINT [FK_VeChuyenBay_HanhKhach]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]') AND type in (N'U'))
DROP TABLE [dbo].[VeChuyenBay]
GO
/****** Object:  Table [dbo].[ChuyenBay]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChuyenBay_SanBay1]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChuyenBay]'))
ALTER TABLE [dbo].[ChuyenBay] DROP CONSTRAINT [FK_ChuyenBay_SanBay1]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChuyenBay_SanBay2]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChuyenBay]'))
ALTER TABLE [dbo].[ChuyenBay] DROP CONSTRAINT [FK_ChuyenBay_SanBay2]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ChuyenBay_so_ghe_da_dat_hang_1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ChuyenBay] DROP CONSTRAINT [DF_ChuyenBay_so_ghe_da_dat_hang_1]
END
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ChuyenBay_so_ghe_da_dat_hang_2]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ChuyenBay] DROP CONSTRAINT [DF_ChuyenBay_so_ghe_da_dat_hang_2]
END
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChuyenBay]') AND type in (N'U'))
DROP TABLE [dbo].[ChuyenBay]
GO
/****** Object:  Table [dbo].[DanhSachGiaSanBay]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSGSB_SanBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachGiaSanBay]'))
ALTER TABLE [dbo].[DanhSachGiaSanBay] DROP CONSTRAINT [FK_DSGSB_SanBay]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_HangVe]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachGiaSanBay]'))
ALTER TABLE [dbo].[DanhSachGiaSanBay] DROP CONSTRAINT [FK_DSSBTG_HangVe]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DanhSachGiaSanBay]') AND type in (N'U'))
DROP TABLE [dbo].[DanhSachGiaSanBay]
GO
/****** Object:  Table [dbo].[HangVe]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HangVe]') AND type in (N'U'))
DROP TABLE [dbo].[HangVe]
GO
/****** Object:  Table [dbo].[HanhKhach]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HanhKhach]') AND type in (N'U'))
DROP TABLE [dbo].[HanhKhach]
GO
/****** Object:  Table [dbo].[SanBay]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SanBay]') AND type in (N'U'))
DROP TABLE [dbo].[SanBay]
GO
/****** Object:  Table [dbo].[ThamSo]    Script Date: 01/15/2018 22:31:46 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ThamSo]') AND type in (N'U'))
DROP TABLE [dbo].[ThamSo]
GO
/****** Object:  Table [dbo].[ThamSo]    Script Date: 01/15/2018 22:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ThamSo]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ThamSo](
	[thoi_gian_bay_toi_thieu] [float] NULL,
	[thoi_gian_dung_toi_da] [int] NULL,
	[thoi_gian_dung_toi_thieu] [int] NULL,
	[so_luong_san_bay_trung_gian_toi_da] [int] NULL,
	[ngay_dat_ve_toi_thieu] [int] NULL
) ON [PRIMARY]
END
GO
INSERT [dbo].[ThamSo] ([thoi_gian_bay_toi_thieu], [thoi_gian_dung_toi_da], [thoi_gian_dung_toi_thieu], [so_luong_san_bay_trung_gian_toi_da], [ngay_dat_ve_toi_thieu]) VALUES (5, 20, 10, 2, 1)
/****** Object:  Table [dbo].[SanBay]    Script Date: 01/15/2018 22:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SanBay]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SanBay](
	[ma_san_bay] [int] IDENTITY(1,1) NOT NULL,
	[ten_san_bay] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ma_san_bay] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[SanBay] ON
INSERT [dbo].[SanBay] ([ma_san_bay], [ten_san_bay]) VALUES (1, N'Tân Sơn Nhât')
INSERT [dbo].[SanBay] ([ma_san_bay], [ten_san_bay]) VALUES (2, N'Nội Bài')
INSERT [dbo].[SanBay] ([ma_san_bay], [ten_san_bay]) VALUES (3, N'Đà Nẵng')
INSERT [dbo].[SanBay] ([ma_san_bay], [ten_san_bay]) VALUES (10, N'Da Lat')
INSERT [dbo].[SanBay] ([ma_san_bay], [ten_san_bay]) VALUES (11, N'da lat')
SET IDENTITY_INSERT [dbo].[SanBay] OFF
/****** Object:  Table [dbo].[HanhKhach]    Script Date: 01/15/2018 22:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HanhKhach]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HanhKhach](
	[ma_hanh_khach] [int] IDENTITY(1,1) NOT NULL,
	[ngay_dat] [datetime] NULL,
	[ten_hanh_khach] [nvarchar](50) NULL,
	[cmnd] [int] NULL,
	[dien_thoai] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[ma_hanh_khach] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HangVe]    Script Date: 01/15/2018 22:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[HangVe]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[HangVe](
	[ma_hang_ve] [int] IDENTITY(1,1) NOT NULL,
	[loai_hang_ve] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[ma_hang_ve] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[HangVe] ON
INSERT [dbo].[HangVe] ([ma_hang_ve], [loai_hang_ve]) VALUES (1, 1)
INSERT [dbo].[HangVe] ([ma_hang_ve], [loai_hang_ve]) VALUES (2, 2)
SET IDENTITY_INSERT [dbo].[HangVe] OFF
/****** Object:  Table [dbo].[DanhSachGiaSanBay]    Script Date: 01/15/2018 22:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DanhSachGiaSanBay]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DanhSachGiaSanBay](
	[ma_san_bay] [int] NOT NULL,
	[ma_hang_ve] [int] NOT NULL,
	[don_gia] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[ma_san_bay] ASC,
	[ma_hang_ve] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[DanhSachGiaSanBay] ([ma_san_bay], [ma_hang_ve], [don_gia]) VALUES (1, 1, 123)
INSERT [dbo].[DanhSachGiaSanBay] ([ma_san_bay], [ma_hang_ve], [don_gia]) VALUES (1, 2, 123)
INSERT [dbo].[DanhSachGiaSanBay] ([ma_san_bay], [ma_hang_ve], [don_gia]) VALUES (2, 1, 123)
INSERT [dbo].[DanhSachGiaSanBay] ([ma_san_bay], [ma_hang_ve], [don_gia]) VALUES (2, 2, 123)
INSERT [dbo].[DanhSachGiaSanBay] ([ma_san_bay], [ma_hang_ve], [don_gia]) VALUES (11, 1, 123)
/****** Object:  Table [dbo].[ChuyenBay]    Script Date: 01/15/2018 22:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ChuyenBay]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ChuyenBay](
	[ma_chuyen_bay] [int] IDENTITY(1,1) NOT NULL,
	[san_bay_di] [int] NULL,
	[san_bay_den] [int] NULL,
	[ngay_gio] [datetime] NULL,
	[thoi_gian_bay] [float] NULL,
	[so_ghe_hang_1] [int] NULL,
	[so_ghe_hang_2] [int] NULL,
	[so_ghe_da_dat_hang_1] [int] NULL CONSTRAINT [DF_ChuyenBay_so_ghe_da_dat_hang_1]  DEFAULT ((0)),
	[so_ghe_da_dat_hang_2] [int] NULL CONSTRAINT [DF_ChuyenBay_so_ghe_da_dat_hang_2]  DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[ma_chuyen_bay] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[ChuyenBay] ON
INSERT [dbo].[ChuyenBay] ([ma_chuyen_bay], [san_bay_di], [san_bay_den], [ngay_gio], [thoi_gian_bay], [so_ghe_hang_1], [so_ghe_hang_2], [so_ghe_da_dat_hang_1], [so_ghe_da_dat_hang_2]) VALUES (96, 1, 1, CAST(0x0000A86900000000 AS DateTime), 1, 2, 3, 0, 0)
INSERT [dbo].[ChuyenBay] ([ma_chuyen_bay], [san_bay_di], [san_bay_den], [ngay_gio], [thoi_gian_bay], [so_ghe_hang_1], [so_ghe_hang_2], [so_ghe_da_dat_hang_1], [so_ghe_da_dat_hang_2]) VALUES (97, 3, 2, CAST(0x0000A86900000000 AS DateTime), 2, 3, 4, 0, 0)
INSERT [dbo].[ChuyenBay] ([ma_chuyen_bay], [san_bay_di], [san_bay_den], [ngay_gio], [thoi_gian_bay], [so_ghe_hang_1], [so_ghe_hang_2], [so_ghe_da_dat_hang_1], [so_ghe_da_dat_hang_2]) VALUES (98, 1, 1, CAST(0x0000A86900000000 AS DateTime), 2, 3, 4, 0, 0)
INSERT [dbo].[ChuyenBay] ([ma_chuyen_bay], [san_bay_di], [san_bay_den], [ngay_gio], [thoi_gian_bay], [so_ghe_hang_1], [so_ghe_hang_2], [so_ghe_da_dat_hang_1], [so_ghe_da_dat_hang_2]) VALUES (99, 11, 11, CAST(0x0000A85F00000000 AS DateTime), 2, 3, 4, 0, 0)
SET IDENTITY_INSERT [dbo].[ChuyenBay] OFF
/****** Object:  Table [dbo].[VeChuyenBay]    Script Date: 01/15/2018 22:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VeChuyenBay](
	[ma_ve] [int] IDENTITY(1,1) NOT NULL,
	[ma_chuyen_bay] [int] NULL,
	[ma_hanh_khach] [int] NULL,
	[ma_hang_ve] [int] NULL,
	[gia_tien] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[ma_ve] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[PhieuDat]    Script Date: 01/15/2018 22:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PhieuDat]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[PhieuDat](
	[ma_phieu_dat] [int] IDENTITY(1,1) NOT NULL,
	[ma_chuyen_bay] [int] NULL,
	[ma_hanh_khach] [int] NULL,
	[ma_hang_ve] [int] NULL,
	[gia_tien] [float] NULL,
	[ngay_dat] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ma_phieu_dat] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DanhSachSanBayTrungGian]    Script Date: 01/15/2018 22:31:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DanhSachSanBayTrungGian]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DanhSachSanBayTrungGian](
	[ma_chuyen_bay] [int] NOT NULL,
	[ma_san_bay] [int] NOT NULL,
	[thoi_gian_dung] [int] NULL,
	[ghi_chu] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[ma_chuyen_bay] ASC,
	[ma_san_bay] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
INSERT [dbo].[DanhSachSanBayTrungGian] ([ma_chuyen_bay], [ma_san_bay], [thoi_gian_dung], [ghi_chu]) VALUES (97, 2, 23, N'ádf')
INSERT [dbo].[DanhSachSanBayTrungGian] ([ma_chuyen_bay], [ma_san_bay], [thoi_gian_dung], [ghi_chu]) VALUES (97, 3, 23, N'ádf')
INSERT [dbo].[DanhSachSanBayTrungGian] ([ma_chuyen_bay], [ma_san_bay], [thoi_gian_dung], [ghi_chu]) VALUES (99, 2, 23, N'ádf')
INSERT [dbo].[DanhSachSanBayTrungGian] ([ma_chuyen_bay], [ma_san_bay], [thoi_gian_dung], [ghi_chu]) VALUES (99, 3, 23, N'ádf')
INSERT [dbo].[DanhSachSanBayTrungGian] ([ma_chuyen_bay], [ma_san_bay], [thoi_gian_dung], [ghi_chu]) VALUES (99, 10, 123, N'ádf')
INSERT [dbo].[DanhSachSanBayTrungGian] ([ma_chuyen_bay], [ma_san_bay], [thoi_gian_dung], [ghi_chu]) VALUES (99, 11, 23, N'ádf')
/****** Object:  ForeignKey [FK_ChuyenBay_SanBay1]    Script Date: 01/15/2018 22:31:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChuyenBay_SanBay1]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChuyenBay]'))
ALTER TABLE [dbo].[ChuyenBay]  WITH CHECK ADD  CONSTRAINT [FK_ChuyenBay_SanBay1] FOREIGN KEY([san_bay_di])
REFERENCES [dbo].[SanBay] ([ma_san_bay])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChuyenBay_SanBay1]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChuyenBay]'))
ALTER TABLE [dbo].[ChuyenBay] CHECK CONSTRAINT [FK_ChuyenBay_SanBay1]
GO
/****** Object:  ForeignKey [FK_ChuyenBay_SanBay2]    Script Date: 01/15/2018 22:31:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChuyenBay_SanBay2]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChuyenBay]'))
ALTER TABLE [dbo].[ChuyenBay]  WITH CHECK ADD  CONSTRAINT [FK_ChuyenBay_SanBay2] FOREIGN KEY([san_bay_den])
REFERENCES [dbo].[SanBay] ([ma_san_bay])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ChuyenBay_SanBay2]') AND parent_object_id = OBJECT_ID(N'[dbo].[ChuyenBay]'))
ALTER TABLE [dbo].[ChuyenBay] CHECK CONSTRAINT [FK_ChuyenBay_SanBay2]
GO
/****** Object:  ForeignKey [FK_DSGSB_SanBay]    Script Date: 01/15/2018 22:31:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSGSB_SanBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachGiaSanBay]'))
ALTER TABLE [dbo].[DanhSachGiaSanBay]  WITH CHECK ADD  CONSTRAINT [FK_DSGSB_SanBay] FOREIGN KEY([ma_san_bay])
REFERENCES [dbo].[SanBay] ([ma_san_bay])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSGSB_SanBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachGiaSanBay]'))
ALTER TABLE [dbo].[DanhSachGiaSanBay] CHECK CONSTRAINT [FK_DSGSB_SanBay]
GO
/****** Object:  ForeignKey [FK_DSSBTG_HangVe]    Script Date: 01/15/2018 22:31:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_HangVe]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachGiaSanBay]'))
ALTER TABLE [dbo].[DanhSachGiaSanBay]  WITH CHECK ADD  CONSTRAINT [FK_DSSBTG_HangVe] FOREIGN KEY([ma_hang_ve])
REFERENCES [dbo].[HangVe] ([ma_hang_ve])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_HangVe]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachGiaSanBay]'))
ALTER TABLE [dbo].[DanhSachGiaSanBay] CHECK CONSTRAINT [FK_DSSBTG_HangVe]
GO
/****** Object:  ForeignKey [FK_DSSBTG_ChuyenBay]    Script Date: 01/15/2018 22:31:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachSanBayTrungGian]'))
ALTER TABLE [dbo].[DanhSachSanBayTrungGian]  WITH CHECK ADD  CONSTRAINT [FK_DSSBTG_ChuyenBay] FOREIGN KEY([ma_chuyen_bay])
REFERENCES [dbo].[ChuyenBay] ([ma_chuyen_bay])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachSanBayTrungGian]'))
ALTER TABLE [dbo].[DanhSachSanBayTrungGian] CHECK CONSTRAINT [FK_DSSBTG_ChuyenBay]
GO
/****** Object:  ForeignKey [FK_DSSBTG_SanBay]    Script Date: 01/15/2018 22:31:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_SanBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachSanBayTrungGian]'))
ALTER TABLE [dbo].[DanhSachSanBayTrungGian]  WITH CHECK ADD  CONSTRAINT [FK_DSSBTG_SanBay] FOREIGN KEY([ma_san_bay])
REFERENCES [dbo].[SanBay] ([ma_san_bay])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_DSSBTG_SanBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[DanhSachSanBayTrungGian]'))
ALTER TABLE [dbo].[DanhSachSanBayTrungGian] CHECK CONSTRAINT [FK_DSSBTG_SanBay]
GO
/****** Object:  ForeignKey [FK_PhieuDat_ChuyenBay]    Script Date: 01/15/2018 22:31:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuDat_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuDat]'))
ALTER TABLE [dbo].[PhieuDat]  WITH CHECK ADD  CONSTRAINT [FK_PhieuDat_ChuyenBay] FOREIGN KEY([ma_chuyen_bay])
REFERENCES [dbo].[ChuyenBay] ([ma_chuyen_bay])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuDat_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuDat]'))
ALTER TABLE [dbo].[PhieuDat] CHECK CONSTRAINT [FK_PhieuDat_ChuyenBay]
GO
/****** Object:  ForeignKey [FK_PhieuDat_HanhKhach]    Script Date: 01/15/2018 22:31:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuDat_HanhKhach]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuDat]'))
ALTER TABLE [dbo].[PhieuDat]  WITH CHECK ADD  CONSTRAINT [FK_PhieuDat_HanhKhach] FOREIGN KEY([ma_hanh_khach])
REFERENCES [dbo].[HanhKhach] ([ma_hanh_khach])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_PhieuDat_HanhKhach]') AND parent_object_id = OBJECT_ID(N'[dbo].[PhieuDat]'))
ALTER TABLE [dbo].[PhieuDat] CHECK CONSTRAINT [FK_PhieuDat_HanhKhach]
GO
/****** Object:  ForeignKey [FK_VeChuyenBay_ChuyenBay]    Script Date: 01/15/2018 22:31:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay]  WITH CHECK ADD  CONSTRAINT [FK_VeChuyenBay_ChuyenBay] FOREIGN KEY([ma_chuyen_bay])
REFERENCES [dbo].[ChuyenBay] ([ma_chuyen_bay])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_ChuyenBay]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay] CHECK CONSTRAINT [FK_VeChuyenBay_ChuyenBay]
GO
/****** Object:  ForeignKey [FK_VeChuyenBay_HangVe]    Script Date: 01/15/2018 22:31:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_HangVe]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay]  WITH CHECK ADD  CONSTRAINT [FK_VeChuyenBay_HangVe] FOREIGN KEY([ma_hang_ve])
REFERENCES [dbo].[HangVe] ([ma_hang_ve])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_HangVe]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay] CHECK CONSTRAINT [FK_VeChuyenBay_HangVe]
GO
/****** Object:  ForeignKey [FK_VeChuyenBay_HanhKhach]    Script Date: 01/15/2018 22:31:46 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_HanhKhach]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay]  WITH CHECK ADD  CONSTRAINT [FK_VeChuyenBay_HanhKhach] FOREIGN KEY([ma_hanh_khach])
REFERENCES [dbo].[HanhKhach] ([ma_hanh_khach])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_VeChuyenBay_HanhKhach]') AND parent_object_id = OBJECT_ID(N'[dbo].[VeChuyenBay]'))
ALTER TABLE [dbo].[VeChuyenBay] CHECK CONSTRAINT [FK_VeChuyenBay_HanhKhach]
GO
