﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using System.Data;

namespace dack.DAO
{
    public class SanBayDAO
    {
        public static List<SanBayDTO> SelectAllSanBay()
        {
            List<SanBayDTO> listSanBay = null;

            string sql = "select * from SanBay";
            DataTable dt = DataProvider.ExecQuery(sql);

            if (dt != null && dt.Rows.Count > 0) {
                listSanBay = new List<SanBayDTO>();

                foreach (DataRow row in dt.Rows) {
                    SanBayDTO sanbay = new SanBayDTO()
                    {
                        ma_san_bay = int.Parse(row.ItemArray[0].ToString()),
                        ten_san_bay = row.ItemArray[1].ToString()
                    };

                    listSanBay.Add(sanbay);
                }
            }

            return listSanBay;
        }

        public static SanBayDTO GetSanBayByID(int SanBayID)
        {
            SanBayDTO sanbay = null;

            string sql = "select * from SanBay where ma_san_bay = " + SanBayID;
            DataTable dt = DataProvider.ExecQuery(sql);

            if (dt != null && dt.Rows.Count > 0) {
                sanbay = new SanBayDTO() { 
                    ma_san_bay = int.Parse(dt.Rows[0].ItemArray[0].ToString()),
                    ten_san_bay = dt.Rows[0].ItemArray[1].ToString()
                };
            }

            return sanbay;
        }

        public static DbAck Insert(SanBayDTO sanbay)
        {
            DbAck result = DbAck.Unknow;

            string sql = "insert into SanBay(ten_san_bay) values(@TenSanBay)";

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@TenSanBay", sanbay.ten_san_bay);

            result = DataProvider.ExecNonQuery(sql, parameter);

            return result;
        }

        public static DbAck Update(SanBayDTO sanbay)
        {
            DbAck result = DbAck.Unknow;

            string sql = "update SanBay set ten_san_bay = @tenSB where ma_san_bay = @maSB";
            Dictionary<string, object> para = new Dictionary<string, object>();
            para.Add("@tenSB", sanbay.ten_san_bay);
            para.Add("@maSB", sanbay.ma_san_bay);

            result = DataProvider.ExecNonQuery(sql, para);

            return result;
        }

        public static DbAck Delete(SanBayDTO sanbay)
        {
            DbAck result = DbAck.Unknow;

            string sql = "delete from SanBay where ma_san_bay = " + sanbay.ma_san_bay;
            result = DataProvider.ExecNonQuery(sql);

            return result;
        }
    }
}
