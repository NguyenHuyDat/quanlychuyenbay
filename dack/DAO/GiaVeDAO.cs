﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using System.Data;
using System.Windows.Forms;

namespace dack.DAO
{
    public class GiaVeDAO
    {
        public static List<GiaVeDTO> SelectAllGiaVe() 
        {
            List<GiaVeDTO> ListGiaVe = null;
            string sql = "select * from DanhSachGiaSanBay";
            DataTable dt = DataProvider.ExecQuery(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                ListGiaVe = new List<GiaVeDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    GiaVeDTO DanhSachGiaSanBay = new GiaVeDTO()
                    {
                        GV_ma_san_bay = int.Parse(row.ItemArray[0].ToString()),
                        GV_ma_hang_ve = int.Parse(row.ItemArray[1].ToString()),
                        GV_Don_Gia = int.Parse(row.ItemArray[2].ToString()),
                    };

                    ListGiaVe.Add(DanhSachGiaSanBay);
                }
            }
            return ListGiaVe;
        }

        public static GiaVeDTO GetGiaVeByID(int MaSB, int MaHV)
        {
            GiaVeDTO giave = null;

            string strquery = "select * from DanhSachGiaSanBay where ma_san_bay = " + MaSB + " and ma_hang_ve = " + MaHV;
            DataTable dt = DataProvider.ExecQuery(strquery);

            List<GiaVeDTO> lstGiaVe = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                lstGiaVe = new List<GiaVeDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    giave = new GiaVeDTO()
                    {
                        GV_ma_san_bay = int.Parse(row.ItemArray[0].ToString()),
                        GV_ma_hang_ve = int.Parse(row.ItemArray[1].ToString()),
                        GV_Don_Gia = int.Parse(row.ItemArray[1].ToString())
                    };

                    lstGiaVe.Add(giave);
                }
            }

            return giave;
        }

        public static DbAck Insert(GiaVeDTO giave)
        {
            DbAck result = DbAck.Unknow;
            string sql = "insert into DanhSachGiaSanBay(ma_san_bay,ma_hang_ve,don_gia) values(@TenSanBay,@mahangve,@dongia)";
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@TenSanBay", giave.GV_ma_san_bay);
            parameter.Add("@mahangve", giave.GV_ma_hang_ve);
            parameter.Add("@dongia", giave.GV_Don_Gia);
            result = DataProvider.ExecNonQuery(sql, parameter);
            return result;
        }

        public static DbAck Update(GiaVeDTO giave)
        {
            DbAck result = DbAck.Unknow;

            //MessageBox.Show(giave.GV_Don_Gia.ToString());
            string sql = "update DanhSachGiaSanBay set don_gia  = " + giave.GV_Don_Gia + " where ma_hang_ve = " + giave.GV_ma_hang_ve +"and ma_san_bay = "+giave.GV_ma_san_bay;

            result = DataProvider.ExecNonQuery(sql);
            
            return result;
        }

        public static DbAck Delete(GiaVeDTO giave)
        {
            DbAck result = DbAck.Unknow;
            string sql = "delete from DanhSachGiaSanBay where ma_hang_ve = " + giave.GV_ma_hang_ve + "and ma_san_bay = " + giave.GV_ma_san_bay;
            result = DataProvider.ExecNonQuery(sql);
            return result;
        }
    }
}
