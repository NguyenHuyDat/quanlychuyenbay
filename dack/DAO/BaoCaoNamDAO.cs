﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using dack.DTO;

namespace dack.DAO
{
    public class BaoCaoNamDAO
    {
        public static List<BaoCaoNamDTO> GetData()
        {
            string strquery = "select year(ChuyenBay.ngay_gio),ChuyenBay.ma_chuyen_bay,(so_ghe_da_dat_hang_1 + so_ghe_da_dat_hang_2 + 0.0)/(so_ghe_hang_1+so_ghe_hang_2 + 0.0) from ChuyenBay,VeChuyenBay,HangVe,DanhSachGiaSanBay group by year(ChuyenBay.ngay_gio),so_ghe_da_dat_hang_1,so_ghe_da_dat_hang_2,so_ghe_hang_1,so_ghe_hang_2,chuyenbay.ma_chuyen_bay";
            DataTable dt = DataProvider.ExecQuery(strquery);

            List<BaoCaoNamDTO> baocao = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                baocao = new List<BaoCaoNamDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    BaoCaoNamDTO bc = new BaoCaoNamDTO()
                    {
                        nam = int.Parse(row.ItemArray[0].ToString()),
                        ma_chuyen_bay = int.Parse(row.ItemArray[1].ToString()),
                        ty_le = float.Parse(row.ItemArray[2].ToString()),
                    };
                    baocao.Add(bc);
                }
            }
            return baocao;
        }

        public static List<BaoCaoNamDTO> FindItem(int id)
        {
            string strquery = "select year(ChuyenBay.ngay_gio),ChuyenBay.ma_chuyen_bay,(so_ghe_da_dat_hang_1 + so_ghe_da_dat_hang_2 + 0.0)/(so_ghe_hang_1+so_ghe_hang_2 + 0.0) from ChuyenBay,VeChuyenBay,HangVe,DanhSachGiaSanBay where year(ChuyenBay.ngay_gio) like '%" + id + "%' group by year(ChuyenBay.ngay_gio),so_ghe_da_dat_hang_1,so_ghe_da_dat_hang_2,so_ghe_hang_1,so_ghe_hang_2,chuyenbay.ma_chuyen_bay";
            DataTable dt = DataProvider.ExecQuery(strquery);

            List<BaoCaoNamDTO> baocao = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                baocao = new List<BaoCaoNamDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    BaoCaoNamDTO bc = new BaoCaoNamDTO()
                    {
                        nam = int.Parse(row.ItemArray[0].ToString()),
                        ma_chuyen_bay = int.Parse(row.ItemArray[1].ToString()),
                        ty_le = float.Parse(row.ItemArray[2].ToString()),
                    };
                    baocao.Add(bc);
                }
            }
            return baocao;
        }
    }
}
