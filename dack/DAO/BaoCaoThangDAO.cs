﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using System.Data;

namespace dack.DAO
{
    public class BaoCaoThangDAO
    {
        public static List<BaoCaoThangDTO> GetData()
        {
            string strquery = "select chuyenbay.ma_chuyen_bay,COUNT(*),ngay_gio,(so_ghe_da_dat_hang_1 + so_ghe_da_dat_hang_2)/(so_ghe_hang_1+so_ghe_hang_2 + 0.0),SUM(don_gia) from ChuyenBay,VeChuyenBay,HangVe,DanhSachGiaSanBay where ChuyenBay.ma_chuyen_bay = VeChuyenBay.ma_chuyen_bay and VeChuyenBay.ma_hang_ve = HangVe.ma_hang_ve and DanhSachGiaSanBay.ma_hang_ve = VeChuyenBay.ma_hang_ve group by ChuyenBay.ma_chuyen_bay,ChuyenBay.ngay_gio,so_ghe_hang_1,so_ghe_hang_2,so_ghe_da_dat_hang_1,so_ghe_da_dat_hang_2,gia_tien";
            DataTable dt = DataProvider.ExecQuery(strquery);

            List<BaoCaoThangDTO> baocao = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                baocao = new List<BaoCaoThangDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    BaoCaoThangDTO bc = new BaoCaoThangDTO()
                    {
                        ma_chuyen_bay = int.Parse(row.ItemArray[0].ToString()),
                        so_luong_ve = int.Parse(row.ItemArray[1].ToString()),
                        ngay_gio = DateTime.Parse(row.ItemArray[2].ToString()),
                        ty_le = float.Parse(row.ItemArray[3].ToString()),
                        doanh_thu = int.Parse(row.ItemArray[4].ToString())
                    };
                    baocao.Add(bc);
                }
            }
            return baocao;
        }

        public static List<BaoCaoThangDTO> FindItem(int id)
        {
            string strquery = "select chuyenbay.ma_chuyen_bay,COUNT(*),ngay_gio,(so_ghe_da_dat_hang_1 + so_ghe_da_dat_hang_2)/(so_ghe_hang_1+so_ghe_hang_2 + 0.0),SUM(don_gia) from ChuyenBay,VeChuyenBay,HangVe,DanhSachGiaSanBay where ChuyenBay.ma_chuyen_bay = VeChuyenBay.ma_chuyen_bay and VeChuyenBay.ma_hang_ve = HangVe.ma_hang_ve and DanhSachGiaSanBay.ma_hang_ve = VeChuyenBay.ma_hang_ve and ChuyenBay.ma_chuyen_bay like '%" + id + "%' group by ChuyenBay.ma_chuyen_bay,ChuyenBay.ngay_gio,so_ghe_hang_1,so_ghe_hang_2,so_ghe_da_dat_hang_1,so_ghe_da_dat_hang_2,gia_tien";
            DataTable dt = DataProvider.ExecQuery(strquery);

            List<BaoCaoThangDTO> baocao = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                baocao = new List<BaoCaoThangDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    BaoCaoThangDTO bc = new BaoCaoThangDTO()
                    {
                        ma_chuyen_bay = int.Parse(row.ItemArray[0].ToString()),
                        so_luong_ve = int.Parse(row.ItemArray[1].ToString()),
                        ngay_gio = DateTime.Parse(row.ItemArray[2].ToString()),
                        ty_le = float.Parse(row.ItemArray[3].ToString()),
                        doanh_thu = int.Parse(row.ItemArray[4].ToString())
                    };
                    baocao.Add(bc);
                }
            }
            return baocao;
        }
        
    }
}



