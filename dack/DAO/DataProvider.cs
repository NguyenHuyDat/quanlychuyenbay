﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using dack.DTO;

namespace dack.DAO
{
    public class DataProvider
    {
        static string connectString = @"Data Source=.;Initial Catalog=DoAnLapTrinhUngDungQuanLy;Integrated Security=True";
        static SqlConnection connection = null;

        public static void OpenConnection() {
            if (connection == null) {
                connection = new SqlConnection(connectString);
            }

            if (connection != null && connection.State == ConnectionState.Closed) {
                connection.Open();
            }
        }

        public static void CloseConnection() {
            if (connection != null && connection.State != ConnectionState.Closed) {
                connection.Close();
            }
        }

        public static DbAck ExecNonQuery(string sql)
        {
            try
            {
                OpenConnection();

                SqlCommand cmd = new SqlCommand(sql, connection);
                cmd.ExecuteNonQuery();
                CloseConnection();
            }
            catch
            {

            }

            return DbAck.Ok;
        }

        public static DbAck ExecNonQuery(string sql, Dictionary<string, object> dic) {
            try{
                OpenConnection();

                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                cmd.CommandText = sql;

                foreach(string key in dic.Keys){
                    cmd.Parameters.AddWithValue(key, dic[key]);
                }
                cmd.ExecuteNonQuery();

                CloseConnection();
            }
            catch{ 
                
            }

            return DbAck.Ok;
        }

        public static DataTable ExecQuery(string sql) {
            OpenConnection();

            DataTable result = new DataTable();
            SqlDataAdapter adapter = new SqlDataAdapter(sql, connectString);
            adapter.Fill(result);

            CloseConnection();

            return result;
        }
    }
}
