﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using System.Data;
using System.Windows.Forms;

namespace dack.DAO
{
    public class SanBayTrungGianDAO
    {
        public static DbAck Insert(SanBayTrungGianDTO sbtg)/*SanBayEntities*/
        {
            DbAck result = DbAck.Unknow;
            string sql = "insert into DanhSachSanBayTrungGian(ma_chuyen_bay,ma_san_bay,thoi_gian_dung,ghi_chu) values(@machuyenbay,@masanbay,@thoigiandung,@ghichu)";
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@machuyenbay", sbtg.ma_chuyen_bay);
            parameter.Add("@masanbay", sbtg.ma_san_bay);
            parameter.Add("@thoigiandung", sbtg.thoi_gian_dung);
            parameter.Add("@ghichu", sbtg.ghi_chu);
            result = DataProvider.ExecNonQuery(sql, parameter);
            return result;
        }

        public static List<SanBayTrungGianDTO> GetSanBayTrungGianByChuyenBayID(int cbid)
        {
            string strquery = "select * from DanhSachSanBayTrungGian where ma_chuyen_bay = " + cbid;
            DataTable dt = DataProvider.ExecQuery(strquery);

            List<SanBayTrungGianDTO> lstsbtg = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                lstsbtg = new List<SanBayTrungGianDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    SanBayTrungGianDTO sbtg = new SanBayTrungGianDTO()
                    {
                        ma_chuyen_bay = int.Parse(row.ItemArray[0].ToString()),
                        ma_san_bay = int.Parse(row.ItemArray[1].ToString()),
                        thoi_gian_dung = int.Parse(row.ItemArray[2].ToString()),
                        ghi_chu = row.ItemArray[3].ToString(),
                    };

                    lstsbtg.Add(sbtg);
                }
            }

            return lstsbtg;
        }

        public static DbAck Delete(SanBayTrungGianDTO sb) { 
            DbAck rs = DbAck.Unknow;

            string sql = "delete from DanhSachSanBayTrungGian where ma_chuyen_bay = "+ sb.ma_chuyen_bay + "and ma_san_bay = " + sb.ma_san_bay;
            rs = DataProvider.ExecNonQuery(sql);

            return rs;
        }

        public static DbAck Update(SanBayTrungGianDTO sb)
        {
            DbAck rs = DbAck.Unknow;

            MessageBox.Show(sb.ma_chuyen_bay.ToString() + " " + sb.ma_san_bay.ToString() + " " + sb.thoi_gian_dung.ToString() + " " + sb.ghi_chu.ToString());

            string sql = "update DanhSachSanBayTrungGian set thoi_gian_dung = " + sb.thoi_gian_dung + ", ghi_chu = '" + sb.ghi_chu + "' where ma_chuyen_bay = " + sb.ma_chuyen_bay + " and ma_san_bay = " + sb.ma_san_bay;
            rs = DataProvider.ExecNonQuery(sql);

            return rs;
        }
    }
}
