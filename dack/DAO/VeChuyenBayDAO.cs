﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using System.Data;
using System.Windows.Forms;

namespace dack.DAO
{
    public class VeChuyenBayDAO
    {
        public static List<VeChuyenBayDTO> SelectAllVeChuyenBay()
        {
            List<VeChuyenBayDTO> listVeChuyenBay = null;

            string sql = "select * from VeChuyenBay";
            DataTable dt = DataProvider.ExecQuery(sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                listVeChuyenBay = new List<VeChuyenBayDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    VeChuyenBayDTO vcb = new VeChuyenBayDTO()
                    {
                        ma_ve = int.Parse(row.ItemArray[0].ToString()),
                        ma_chuyen_bay = int.Parse(row.ItemArray[1].ToString()),
                        ma_hang_ve = int.Parse(row.ItemArray[2].ToString()),
                        ma_hanh_khach = int.Parse(row.ItemArray[3].ToString()),
                        gia_tien = int.Parse(row.ItemArray[4].ToString()),
                    };

                    listVeChuyenBay.Add(vcb);
                }
            }

            return listVeChuyenBay;
        }

        public static VeChuyenBayDTO GetChuyenBayByID(int vcbID)
        {
            VeChuyenBayDTO vcb = null;

            string strquery = "select * from VeChuyenBay where ma_ve = " + vcbID;
            DataTable dt = DataProvider.ExecQuery(strquery);

            List<VeChuyenBayDTO> lstVCB = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                lstVCB = new List<VeChuyenBayDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    vcb = new VeChuyenBayDTO()
                    {
                        ma_ve = int.Parse(row.ItemArray[0].ToString()),
                        ma_chuyen_bay = int.Parse(row.ItemArray[1].ToString()),
                        ma_hanh_khach = int.Parse(row.ItemArray[2].ToString()),
                        ma_hang_ve = int.Parse(row.ItemArray[3].ToString()),
                        gia_tien = int.Parse(row.ItemArray[4].ToString()),
                    };

                    lstVCB.Add(vcb);
                }
            }

            return vcb;
        }

        public static DbAck Insert(VeChuyenBayDTO vcb)
        {
            MessageBox.Show(vcb.ma_chuyen_bay.ToString() + vcb.ma_hang_ve.ToString() + vcb.ma_hanh_khach.ToString() + vcb.gia_tien.ToString());

            DbAck result = DbAck.Unknow;

            string sql = "insert into VeChuyenBay(ma_chuyen_bay, ma_hanh_khach, ma_hang_ve, gia_tien) values(@mcb, @mkh, @mhv, @gt)";

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@mcb", vcb.ma_chuyen_bay);
            parameter.Add("@mhk", vcb.ma_hanh_khach);
            parameter.Add("@mhv", vcb.ma_hang_ve);
            parameter.Add("@gt", vcb.gia_tien);

            result = DataProvider.ExecNonQuery(sql, parameter);

            return result;
        }

        public static DbAck Update(VeChuyenBayDTO vcb)
        {
            DbAck result = DbAck.Unknow;

            return result;
        }

        public static DbAck Delete(VeChuyenBayDTO vcb)
        {
            DbAck result = DbAck.Unknow;

            return result;
        }
    }
}
