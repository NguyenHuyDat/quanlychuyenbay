﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using System.Data;

namespace dack.DAO
{
    class HanhKhachDAO
    {

        public static List<HanhKhachDTO> SelectAllHanhKhach()
        {
            List<HanhKhachDTO> listHanhKhach = null;

            string sql = "select * from HanhKhach";
            DataTable dt = DataProvider.ExecQuery(sql);

            if (dt != null && dt.Rows.Count > 0)
            {
                listHanhKhach = new List<HanhKhachDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    HanhKhachDTO HanhKhach = new HanhKhachDTO()
                    {
                        ma_hanh_khach = int.Parse(row.ItemArray[0].ToString()),
                        ngay_dat = DateTime.Parse(row.ItemArray[1].ToString()),
                        ten_hanh_khach = row.ItemArray[2].ToString(),
                        cmnd = int.Parse(row.ItemArray[3].ToString()),
                        dien_thoai = int.Parse(row.ItemArray[4].ToString())
                    };

                    listHanhKhach.Add(HanhKhach);
                }
            }

            return listHanhKhach;
        }


        public static HanhKhachDTO GetHanhKhachByID(int HanhKhachID)
        {
            HanhKhachDTO HanhKhach = null;

            string sql = "select * from HanhKhach";
            DataTable dt = DataProvider.ExecQuery(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                HanhKhach = new HanhKhachDTO()
                {
                    ma_hanh_khach = int.Parse(dt.Rows[0].ItemArray[0].ToString()),
                    ngay_dat = DateTime.Parse(dt.Rows[0].ItemArray[1].ToString()),
                    ten_hanh_khach = dt.Rows[0].ItemArray[2].ToString(),
                    cmnd = int.Parse(dt.Rows[0].ItemArray[3].ToString()),
                    dien_thoai = int.Parse(dt.Rows[0].ItemArray[4].ToString())
                };
            }

            return HanhKhach;
        }


        public static DbAck Update(HanhKhachDTO HanhKhach)
        {
            DbAck result = DbAck.Unknow;

            string sql = "update HanhKhach set ngay_dat= @ngaydat,ten_hanh_khach= @tenhanhkhach, cmnd=@cmnd, dien_thoai=@dienthoai where ma_hanh_khach = @mahanhkhach ";

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@mahanhkhach", HanhKhach.ma_hanh_khach);
            parameter.Add("@ngaydat", HanhKhach.ngay_dat);
            parameter.Add("@tenhanhkhach", HanhKhach.ten_hanh_khach);
            parameter.Add("@cmnd", HanhKhach.cmnd);
            parameter.Add("@dienthoai", HanhKhach.dien_thoai);

            result = DataProvider.ExecNonQuery(sql, parameter);

            return result;
        }


        public static DbAck Delete(HanhKhachDTO HanhKhach)
        {
            DbAck result = DbAck.Unknow;

            string sql = "delete HanhKhach where ma_hanh_khach=@mahanhkhach";

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@mahanhkhach", HanhKhach.ma_hanh_khach);

            result = DataProvider.ExecNonQuery(sql, parameter);

            return result;
        }


        internal static DbAck Insert(HanhKhachDTO HanhKhach)
        {

            DbAck result = DbAck.Unknow;

            string sql = "insert into hanhkhach(ngay_dat,ten_hanh_khach,cmnd,dien_thoai) values(@ngaydat,@tenhanhkhach,@cmnd,@dienthoai)";

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@mahanhkhach", HanhKhach.ma_hanh_khach);
            parameter.Add("@ngaydat", HanhKhach.ngay_dat);
            parameter.Add("@tenhanhkhach", HanhKhach.ten_hanh_khach);
            parameter.Add("@cmnd", HanhKhach.cmnd);
            parameter.Add("@dienthoai", HanhKhach.dien_thoai);

            result = DataProvider.ExecNonQuery(sql, parameter);

            return result;
        }
    }
}
