﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using System.Data;
using System.Windows.Forms;

namespace dack.DAO
{
    public class HangVeDAO
    {
        public static List<HangVeDTO> SelectAllHangVe()
        {
            List<HangVeDTO> listHangVe = null;
            string sql = "select * from HangVe";
            DataTable dt = DataProvider.ExecQuery(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                listHangVe = new List<HangVeDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    HangVeDTO hangve = new HangVeDTO()
                    {
                        ma_hang_ve = int.Parse(row.ItemArray[0].ToString()),
                        loai_hang_ve = row.ItemArray[1].ToString()
                    };

                    listHangVe.Add(hangve);
                }
            }
            return listHangVe;
        }

        public static HangVeDTO GethangVeByID(int hangVeID)
        {
            HangVeDTO hangVe = null;

            string strquery = "select * from HangVe where ma_hang_ve = " + hangVeID;
            DataTable dt = DataProvider.ExecQuery(strquery);

            List<HangVeDTO> lstHV = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                lstHV = new List<HangVeDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    hangVe = new HangVeDTO()
                    {
                        ma_hang_ve = int.Parse(row.ItemArray[0].ToString()),
                        loai_hang_ve = row.ItemArray[1].ToString(),
                    };

                    lstHV.Add(hangVe);
                }
            }

            return hangVe;
        }

        public static DbAck Insert(HangVeDTO hangve)
        {
            DbAck result = DbAck.Unknow;
            string sql = "insert into HangVe(loai_hang_ve) values(@loaihangve)";
            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@loaihangve",hangve.loai_hang_ve);
            result = DataProvider.ExecNonQuery(sql, parameter);
            return result;
        }

        public static DbAck Update(HangVeDTO hangve)
        {
            DbAck result = DbAck.Unknow;
            string sql = "update HangVe Set loai_hang_ve = " + hangve.loai_hang_ve + "where ma_hang_ve = " + hangve.ma_hang_ve;
            result = DataProvider.ExecNonQuery(sql);
            return result;
        }

        public static DbAck Delete(HangVeDTO hangve)
        {
            DbAck result = DbAck.Unknow;
            string sql = "delete HangVe where ma_hang_ve = " + hangve.ma_hang_ve;
            result = DataProvider.ExecNonQuery(sql);
            return result;
        }
    }
}
