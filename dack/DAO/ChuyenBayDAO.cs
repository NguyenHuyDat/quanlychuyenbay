﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using System.Data;
using System.Windows.Forms;

namespace dack.DAO
{
    public class ChuyenBayDAO
    {
        public static List<ChuyenBayDTO> SelectAllChuyenBay()
        {
            string strquery = "select * from ChuyenBay order by ma_chuyen_bay desc";
            DataTable dt = DataProvider.ExecQuery(strquery);

            List<ChuyenBayDTO> lstcb = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                lstcb = new List<ChuyenBayDTO>();

                foreach (DataRow row in dt.Rows)
                {
                    ChuyenBayDTO cb = new ChuyenBayDTO()
                    {
                        ma_chuyen_bay = int.Parse(row.ItemArray[0].ToString()),
                        san_bay_di = int.Parse(row.ItemArray[1].ToString()),
                        san_bay_den = int.Parse(row.ItemArray[2].ToString()),
                        ngay_gio = DateTime.Parse(row.ItemArray[3].ToString()),
                        thoi_gian_bay = float.Parse(row.ItemArray[4].ToString()),
                        so_ghe_hang1 = int.Parse(row.ItemArray[5].ToString()),
                        so_ghe_hang2 = int.Parse(row.ItemArray[6].ToString()),
                        so_ghe_da_dat_hang1 = int.Parse(row.ItemArray[7].ToString()),
                        so_ghe_da_dat_hang2 = int.Parse(row.ItemArray[8].ToString()),
                    };

                    lstcb.Add(cb);
                }
            }

            return lstcb;
        }

        public static ChuyenBayDTO GetChuyenBayByID(int id)
        {
            ChuyenBayDTO cb = null;

            string sql = "select * from ChuyenBay where ma_chuyen_bay = " + id;
            DataTable dt = DataProvider.ExecQuery(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                cb = new ChuyenBayDTO()
                {

                    ma_chuyen_bay = int.Parse(dt.Rows[0].ItemArray[0].ToString()),
                    san_bay_di = int.Parse(dt.Rows[0].ItemArray[1].ToString()),
                    san_bay_den = int.Parse(dt.Rows[0].ItemArray[2].ToString()),
                    ngay_gio = DateTime.Parse(dt.Rows[0].ItemArray[3].ToString()),
                    thoi_gian_bay = float.Parse(dt.Rows[0].ItemArray[4].ToString()),
                    so_ghe_hang1 = int.Parse(dt.Rows[0].ItemArray[5].ToString()),
                    so_ghe_hang2 = int.Parse(dt.Rows[0].ItemArray[6].ToString()),
                    so_ghe_da_dat_hang1 = int.Parse(dt.Rows[0].ItemArray[7].ToString()),
                    so_ghe_da_dat_hang2 = int.Parse(dt.Rows[0].ItemArray[8].ToString()),
                };
            }

            return cb;
        }

        public static DbAck Insert(ChuyenBayDTO chuyenbay)
        {
            DbAck result = DbAck.Unknow;
            string sql = "insert into ChuyenBay(san_bay_di, san_bay_den,so_ghe_hang_1,so_ghe_hang_2,thoi_gian_bay,ngay_gio) values(@sanbaydi, @sanbayden, @slghehang1,@slghehang2,@thoigianbay,@ngaygio)";

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@sanbaydi", chuyenbay.san_bay_di);
            parameter.Add("@sanbayden", chuyenbay.san_bay_den);
            parameter.Add("@slghehang1", chuyenbay.so_ghe_hang1);
            parameter.Add("@slghehang2", chuyenbay.so_ghe_hang2);
            parameter.Add("@thoigianbay", chuyenbay.thoi_gian_bay);
            parameter.Add("@ngaygio", chuyenbay.ngay_gio);

            result = DataProvider.ExecNonQuery(sql, parameter);
            return result;
        }

        public static DbAck Update(ChuyenBayDTO chuyenbay)
        {
            DbAck result = DbAck.Unknow;

            string sql = "update ChuyenBay set san_bay_di = @sanbaydi, san_bay_den = @sanbayden, so_ghe_hang_1 = @slghehang1, so_ghe_hang_2 = @slghehang2, thoi_gian_bay = @thoigianbay, ngay_gio = @ngaygio where ma_chuyen_bay = @machuyenbay";

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@sanbaydi", chuyenbay.san_bay_di);
            parameter.Add("@sanbayden", chuyenbay.san_bay_den);
            parameter.Add("@slghehang1", chuyenbay.so_ghe_hang1);
            parameter.Add("@slghehang2", chuyenbay.so_ghe_hang2);
            parameter.Add("@thoigianbay", chuyenbay.thoi_gian_bay);
            parameter.Add("@ngaygio", chuyenbay.ngay_gio);
            parameter.Add("@machuyenbay", chuyenbay.ma_chuyen_bay);

            result = DataProvider.ExecNonQuery(sql, parameter);

            return result;
        }

        public static DbAck Delete(ChuyenBayDTO cb)
        {
            DbAck result = DbAck.Unknow;

            string sql = "delete ChuyenBay where ma_chuyen_bay = " + cb.ma_chuyen_bay;
            result = DataProvider.ExecNonQuery(sql);

            return result;
        }

        public static ChuyenBayDTO getIDMax()
        {
            ChuyenBayDTO result = null;

            string sql = "select top 1 * from ChuyenBay order by ma_chuyen_bay desc";
            DataTable dt = DataProvider.ExecQuery(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    result = new ChuyenBayDTO();
                    result.ma_chuyen_bay = int.Parse(row.ItemArray[0].ToString());
                }
            }
            return result;
        }
    }
}
