﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using System.Data;
namespace dack.DAO
{
    class ThamSoDAO
    {
        public static ThamSoDTO SelectAllThamSo()
        {
            ThamSoDTO thamso = null;

            string sql = "select * from ThamSo";
            DataTable dt = DataProvider.ExecQuery(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                thamso = new ThamSoDTO()
                {
                    thoi_gian_bay_toi_thieu = float.Parse(dt.Rows[0].ItemArray[0].ToString()),
                    thoi_gian_dung_toi_da = int.Parse(dt.Rows[0].ItemArray[1].ToString()),
                    thoi_gian_dung_toi_thieu = int.Parse(dt.Rows[0].ItemArray[2].ToString()),
                    so_luong_san_bay_trung_gian_toi_da = int.Parse(dt.Rows[0].ItemArray[3].ToString()),
                    ngay_dat_ve_toi_thieu = int.Parse(dt.Rows[0].ItemArray[4].ToString())
                };
            }

            return thamso;
        }

        public static ThamSoDTO GetThamSoByID(int ThamSoID)
        {
            ThamSoDTO thamso = null;

            string sql = "select * from ThamSo";
            DataTable dt = DataProvider.ExecQuery(sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                thamso = new ThamSoDTO()
                {
                    thoi_gian_bay_toi_thieu = float.Parse(dt.Rows[0].ItemArray[0].ToString()),
                    thoi_gian_dung_toi_da = int.Parse(dt.Rows[0].ItemArray[1].ToString()),
                    thoi_gian_dung_toi_thieu = int.Parse(dt.Rows[0].ItemArray[2].ToString()),
                    so_luong_san_bay_trung_gian_toi_da = int.Parse(dt.Rows[0].ItemArray[3].ToString()),
                    ngay_dat_ve_toi_thieu = int.Parse(dt.Rows[0].ItemArray[4].ToString())
                };
            }

            return thamso;
        }


        public static DbAck Update(ThamSoDTO thamSo)
        {
            DbAck result = DbAck.Unknow;

            string sql = "update thamso set ngay_dat_ve_toi_thieu = @ngayDatVeToiThieu,so_luong_san_bay_trung_gian_toi_da= @soLuongSBTrungGianToiDa,thoi_gian_bay_toi_thieu= @thoiGianBayToiThieu, thoi_gian_dung_toi_thieu=@thoiGianDungToiThieu, thoi_gian_dung_toi_da=@thoiGianDungToiDa ";

            Dictionary<string, object> parameter = new Dictionary<string, object>();
            parameter.Add("@ngayDatVeToiThieu", thamSo.ngay_dat_ve_toi_thieu);
            parameter.Add("@soLuongSBTrungGianToiDa", thamSo.so_luong_san_bay_trung_gian_toi_da);
            parameter.Add("@thoiGianBayToiThieu", thamSo.thoi_gian_bay_toi_thieu);
            parameter.Add("@thoiGianDungToiDa", thamSo.thoi_gian_dung_toi_da);
            parameter.Add("@thoiGianDungToiThieu", thamSo.thoi_gian_dung_toi_thieu);

            result = DataProvider.ExecNonQuery(sql, parameter);

            return result;
        }

    }
}
