﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dack.DTO
{
    public class VeChuyenBayDTO
    {
        public int ma_ve { get; set; }
        public int ma_chuyen_bay { get; set; }
        public int ma_hanh_khach { get; set; }
        public int ma_hang_ve { get; set; }
        public int gia_tien { get; set; }
    }
}
