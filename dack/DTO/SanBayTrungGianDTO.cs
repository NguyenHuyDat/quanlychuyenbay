﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dack.DTO
{
    public class SanBayTrungGianDTO
    {
        public int ma_san_bay { get; set; }
        public int ma_chuyen_bay { get; set; }
        public int thoi_gian_dung { get; set; }
        public string ghi_chu { get; set; }
    }
}
