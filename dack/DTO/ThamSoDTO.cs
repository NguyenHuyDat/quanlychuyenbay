﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dack.DTO
{
    class ThamSoDTO
    {
        public float thoi_gian_bay_toi_thieu { get; set; }
        public int thoi_gian_dung_toi_da { get; set; }
        public int thoi_gian_dung_toi_thieu { get; set; }
        public int so_luong_san_bay_trung_gian_toi_da { get; set; }
        public int ngay_dat_ve_toi_thieu { get; set; }

    }
}
