﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dack.DTO
{
    class HanhKhachDTO
    {
        public int ma_hanh_khach { get; set; }
        public DateTime ngay_dat { get; set; }
        public string ten_hanh_khach { get; set; }
        public int cmnd { get; set; }
        public int dien_thoai { get; set; }
    }
}
