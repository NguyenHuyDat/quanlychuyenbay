﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dack.DTO
{
    public class HangVeDTO
    {
        public int ma_hang_ve { get; set; }
        public string loai_hang_ve { get; set; }
    }
}
