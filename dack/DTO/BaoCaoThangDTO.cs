﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dack.DTO
{
    public class BaoCaoThangDTO
    {
        public int ma_chuyen_bay { get; set; }
        public int so_luong_ve { get; set; }
        public DateTime ngay_gio { get; set; }
        public float ty_le { get; set; }
        public int doanh_thu { get; set; }
    }
}
