﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dack.DTO
{
    public class SanBayDTO
    {
        public int ma_san_bay { get; set; }
        public string ten_san_bay { get; set; }
    }
}
