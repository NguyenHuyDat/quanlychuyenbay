﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dack.DTO
{
    public class ChuyenBayDTO
    {
        private int macb;
        public int ma_chuyen_bay
        {
            get { return macb; }
            set { macb = value; }
        }

        private int sbdi;
        public int san_bay_di
        {
            get { return sbdi; }
            set { sbdi = value; }
        }

        private int sbden;
        public int san_bay_den
        {
            get { return sbden; }
            set { sbden = value; }
        }

        private DateTime ngaygio;
        public DateTime ngay_gio
        {
            get { return ngaygio; }
            set { ngaygio = value; }
        }

        private float thoigianbay;
        public float thoi_gian_bay
        {
            get { return thoigianbay; }
            set { thoigianbay = value; }
        }

        private int soghehang1;
        public int so_ghe_hang1
        {
            get { return soghehang1; }
            set { soghehang1 = value; }
        }

        private int soghehang2;
        public int so_ghe_hang2
        {
            get { return soghehang2; }
            set { soghehang2 = value; }
        }

        private int soghedadathang1;
        public int so_ghe_da_dat_hang1
        {
            get { return soghedadathang1; }
            set { soghedadathang1 = value; }
        }

        private int soghedadathang2;
        public int so_ghe_da_dat_hang2
        {
            get { return soghedadathang2; }
            set { soghedadathang2 = value; }
        }
    }
}
