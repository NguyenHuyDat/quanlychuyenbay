﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dack.DTO
{
    public class BaoCaoNamDTO
    {
        public int nam { get; set; }
        public int ma_chuyen_bay { get; set; }
        public float ty_le { get; set; }
    }
}
