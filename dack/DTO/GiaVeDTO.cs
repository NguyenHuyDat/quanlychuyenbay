﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace dack.DTO
{
    public class GiaVeDTO
    {
        public int GV_ma_san_bay { get; set; }
        public int GV_ma_hang_ve { get; set; }
        public int GV_Don_Gia { get; set; }
    }
}
