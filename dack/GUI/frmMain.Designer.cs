﻿namespace dack.GUI
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.hệThốngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.quảnLýDanhMụcToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSanBay = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiVeChuyenBay = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDatChoChuyenBay = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiGiaVe = new System.Windows.Forms.ToolStripMenuItem();
            this.tmsi_HangVe = new System.Windows.Forms.ToolStripMenuItem();
            this.hànhKháchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiChuyenBay = new System.Windows.Forms.ToolStripMenuItem();
            this.thayĐToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thamSốToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.báoCáoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiThang = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiNam = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hướngDẫnSửDụngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thôngTinPhầnMềmToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgvPDC = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgvVeCB = new System.Windows.Forms.DataGridView();
            this.btn_TP = new System.Windows.Forms.Button();
            this.btn_TV = new System.Windows.Forms.Button();
            this.dtpND = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.txtGT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cb_hv = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cb_hk = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cb_cb = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPDC)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVeCB)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hệThốngToolStripMenuItem,
            this.quảnLýDanhMụcToolStripMenuItem,
            this.thayĐToolStripMenuItem,
            this.báoCáoToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(935, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // hệThốngToolStripMenuItem
            // 
            this.hệThốngToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiExit});
            this.hệThốngToolStripMenuItem.Name = "hệThốngToolStripMenuItem";
            this.hệThốngToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.hệThốngToolStripMenuItem.Text = "Hệ Thống";
            // 
            // tsmiExit
            // 
            this.tsmiExit.Name = "tsmiExit";
            this.tsmiExit.Size = new System.Drawing.Size(92, 22);
            this.tsmiExit.Text = "Exit";
            // 
            // quảnLýDanhMụcToolStripMenuItem
            // 
            this.quảnLýDanhMụcToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiSanBay,
            this.tsmiVeChuyenBay,
            this.tsmiDatChoChuyenBay,
            this.tsmiGiaVe,
            this.tmsi_HangVe,
            this.hànhKháchToolStripMenuItem,
            this.tsmiChuyenBay});
            this.quảnLýDanhMụcToolStripMenuItem.Name = "quảnLýDanhMụcToolStripMenuItem";
            this.quảnLýDanhMụcToolStripMenuItem.Size = new System.Drawing.Size(117, 20);
            this.quảnLýDanhMụcToolStripMenuItem.Text = "Quản lý danh mục";
            // 
            // tsmiSanBay
            // 
            this.tsmiSanBay.Name = "tsmiSanBay";
            this.tsmiSanBay.Size = new System.Drawing.Size(179, 22);
            this.tsmiSanBay.Text = "Sân Bay";
            // 
            // tsmiVeChuyenBay
            // 
            this.tsmiVeChuyenBay.Name = "tsmiVeChuyenBay";
            this.tsmiVeChuyenBay.Size = new System.Drawing.Size(179, 22);
            this.tsmiVeChuyenBay.Text = "Vé chuyến bay";
            // 
            // tsmiDatChoChuyenBay
            // 
            this.tsmiDatChoChuyenBay.Name = "tsmiDatChoChuyenBay";
            this.tsmiDatChoChuyenBay.Size = new System.Drawing.Size(179, 22);
            this.tsmiDatChoChuyenBay.Text = "Đặt chỗ chuyến bay";
            // 
            // tsmiGiaVe
            // 
            this.tsmiGiaVe.Name = "tsmiGiaVe";
            this.tsmiGiaVe.Size = new System.Drawing.Size(179, 22);
            this.tsmiGiaVe.Text = "Giá Vé";
            // 
            // tmsi_HangVe
            // 
            this.tmsi_HangVe.Name = "tmsi_HangVe";
            this.tmsi_HangVe.Size = new System.Drawing.Size(179, 22);
            this.tmsi_HangVe.Text = "Hạng Vé";
            // 
            // hànhKháchToolStripMenuItem
            // 
            this.hànhKháchToolStripMenuItem.Name = "hànhKháchToolStripMenuItem";
            this.hànhKháchToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.hànhKháchToolStripMenuItem.Text = "Hành Khách";
            this.hànhKháchToolStripMenuItem.Click += new System.EventHandler(this.hànhKháchToolStripMenuItem_Click);
            // 
            // tsmiChuyenBay
            // 
            this.tsmiChuyenBay.Name = "tsmiChuyenBay";
            this.tsmiChuyenBay.Size = new System.Drawing.Size(179, 22);
            this.tsmiChuyenBay.Text = "Chuyến Bay";
            this.tsmiChuyenBay.Click += new System.EventHandler(this.tsmiChuyenBay_Click_1);
            // 
            // thayĐToolStripMenuItem
            // 
            this.thayĐToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.thamSốToolStripMenuItem});
            this.thayĐToolStripMenuItem.Name = "thayĐToolStripMenuItem";
            this.thayĐToolStripMenuItem.Size = new System.Drawing.Size(115, 20);
            this.thayĐToolStripMenuItem.Text = "Thay đổi quy định";
            // 
            // thamSốToolStripMenuItem
            // 
            this.thamSốToolStripMenuItem.Name = "thamSốToolStripMenuItem";
            this.thamSốToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.thamSốToolStripMenuItem.Text = "Tham Số";
            this.thamSốToolStripMenuItem.Click += new System.EventHandler(this.thamSốToolStripMenuItem_Click);
            // 
            // báoCáoToolStripMenuItem
            // 
            this.báoCáoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiThang,
            this.tsmiNam});
            this.báoCáoToolStripMenuItem.Name = "báoCáoToolStripMenuItem";
            this.báoCáoToolStripMenuItem.Size = new System.Drawing.Size(122, 20);
            this.báoCáoToolStripMenuItem.Text = "Báo cáo - Thống kê";
            // 
            // tsmiThang
            // 
            this.tsmiThang.Name = "tsmiThang";
            this.tsmiThang.Size = new System.Drawing.Size(108, 22);
            this.tsmiThang.Text = "Tháng";
            // 
            // tsmiNam
            // 
            this.tsmiNam.Name = "tsmiNam";
            this.tsmiNam.Size = new System.Drawing.Size(108, 22);
            this.tsmiNam.Text = "Năm";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hướngDẫnSửDụngToolStripMenuItem,
            this.thôngTinPhầnMềmToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // hướngDẫnSửDụngToolStripMenuItem
            // 
            this.hướngDẫnSửDụngToolStripMenuItem.Name = "hướngDẫnSửDụngToolStripMenuItem";
            this.hướngDẫnSửDụngToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.hướngDẫnSửDụngToolStripMenuItem.Text = "Hướng dẫn sử dụng";
            // 
            // thôngTinPhầnMềmToolStripMenuItem
            // 
            this.thôngTinPhầnMềmToolStripMenuItem.Name = "thôngTinPhầnMềmToolStripMenuItem";
            this.thôngTinPhầnMềmToolStripMenuItem.Size = new System.Drawing.Size(187, 22);
            this.thôngTinPhầnMềmToolStripMenuItem.Text = "Thông tin phần mềm";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btn_TP);
            this.groupBox1.Controls.Add(this.btn_TV);
            this.groupBox1.Controls.Add(this.dtpND);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txtGT);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cb_hv);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cb_hk);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cb_cb);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(911, 406);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Vé / Phiếu đặt chuyến bay";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgvPDC);
            this.groupBox3.Location = new System.Drawing.Point(359, 196);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(546, 207);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Danh Sách Phiếu";
            // 
            // dgvPDC
            // 
            this.dgvPDC.AllowUserToAddRows = false;
            this.dgvPDC.AllowUserToDeleteRows = false;
            this.dgvPDC.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPDC.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvPDC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvPDC.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dgvPDC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPDC.EnableHeadersVisualStyles = false;
            this.dgvPDC.Location = new System.Drawing.Point(3, 16);
            this.dgvPDC.MultiSelect = false;
            this.dgvPDC.Name = "dgvPDC";
            this.dgvPDC.ReadOnly = true;
            this.dgvPDC.RowHeadersVisible = false;
            this.dgvPDC.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvPDC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPDC.Size = new System.Drawing.Size(540, 188);
            this.dgvPDC.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ma_san_bay";
            this.dataGridViewTextBoxColumn1.FillWeight = 40F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Mã sân bay";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "ten_san_bay";
            this.dataGridViewTextBoxColumn2.HeaderText = "Tên sân bay";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvVeCB);
            this.groupBox2.Location = new System.Drawing.Point(359, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(546, 163);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh Sách Vé";
            // 
            // dgvVeCB
            // 
            this.dgvVeCB.AllowUserToAddRows = false;
            this.dgvVeCB.AllowUserToDeleteRows = false;
            this.dgvVeCB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvVeCB.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvVeCB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVeCB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvVeCB.EnableHeadersVisualStyles = false;
            this.dgvVeCB.Location = new System.Drawing.Point(3, 16);
            this.dgvVeCB.MultiSelect = false;
            this.dgvVeCB.Name = "dgvVeCB";
            this.dgvVeCB.ReadOnly = true;
            this.dgvVeCB.RowHeadersVisible = false;
            this.dgvVeCB.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvVeCB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVeCB.Size = new System.Drawing.Size(540, 144);
            this.dgvVeCB.TabIndex = 7;
            // 
            // btn_TP
            // 
            this.btn_TP.Location = new System.Drawing.Point(199, 256);
            this.btn_TP.Name = "btn_TP";
            this.btn_TP.Size = new System.Drawing.Size(130, 23);
            this.btn_TP.TabIndex = 11;
            this.btn_TP.Text = "Thêm Phiếu Đặt Chỗ";
            this.btn_TP.UseVisualStyleBackColor = true;
            // 
            // btn_TV
            // 
            this.btn_TV.Location = new System.Drawing.Point(23, 256);
            this.btn_TV.Name = "btn_TV";
            this.btn_TV.Size = new System.Drawing.Size(133, 23);
            this.btn_TV.TabIndex = 10;
            this.btn_TV.Text = "Thêm Vé Chuyên Bay";
            this.btn_TV.UseVisualStyleBackColor = true;
            // 
            // dtpND
            // 
            this.dtpND.Location = new System.Drawing.Point(106, 196);
            this.dtpND.Name = "dtpND";
            this.dtpND.Size = new System.Drawing.Size(223, 20);
            this.dtpND.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Ngày Đặt:";
            // 
            // txtGT
            // 
            this.txtGT.Enabled = false;
            this.txtGT.Location = new System.Drawing.Point(106, 153);
            this.txtGT.Name = "txtGT";
            this.txtGT.Size = new System.Drawing.Size(223, 20);
            this.txtGT.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 156);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Giá tiền:";
            // 
            // cb_hv
            // 
            this.cb_hv.FormattingEnabled = true;
            this.cb_hv.Location = new System.Drawing.Point(106, 117);
            this.cb_hv.Name = "cb_hv";
            this.cb_hv.Size = new System.Drawing.Size(223, 21);
            this.cb_hv.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Hạng Vé:";
            // 
            // cb_hk
            // 
            this.cb_hk.FormattingEnabled = true;
            this.cb_hk.Location = new System.Drawing.Point(106, 79);
            this.cb_hk.Name = "cb_hk";
            this.cb_hk.Size = new System.Drawing.Size(223, 21);
            this.cb_hk.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Khách hàng: ";
            // 
            // cb_cb
            // 
            this.cb_cb.FormattingEnabled = true;
            this.cb_cb.Location = new System.Drawing.Point(106, 37);
            this.cb_cb.Name = "cb_cb";
            this.cb_cb.Size = new System.Drawing.Size(223, 21);
            this.cb_cb.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Chuyến Bay: ";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(935, 451);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMain";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPDC)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvVeCB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hệThốngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quảnLýDanhMụcToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thayĐToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiSanBay;
        private System.Windows.Forms.ToolStripMenuItem tsmiVeChuyenBay;
        private System.Windows.Forms.ToolStripMenuItem tsmiDatChoChuyenBay;
        private System.Windows.Forms.ToolStripMenuItem báoCáoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiThang;
        private System.Windows.Forms.ToolStripMenuItem tsmiNam;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hướngDẫnSửDụngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thôngTinPhầnMềmToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiExit;
        private System.Windows.Forms.ToolStripMenuItem tsmiGiaVe;
        private System.Windows.Forms.ToolStripMenuItem tmsi_HangVe;
        private System.Windows.Forms.ToolStripMenuItem hànhKháchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem thamSốToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiChuyenBay;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_TP;
        private System.Windows.Forms.Button btn_TV;
        private System.Windows.Forms.DateTimePicker dtpND;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtGT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cb_hv;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cb_hk;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cb_cb;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvPDC;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridView dgvVeCB;
    }
}