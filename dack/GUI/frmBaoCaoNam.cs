﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dack.BUS;
using dack.DTO;

namespace dack.GUI
{
    public partial class frmBaoCaoNam : Form
    {
        public frmBaoCaoNam()
        {
            InitializeComponent();
        }

        private void BaoCaoNam_Load(object sender, EventArgs e)
        {
            dgvNam.DataSource = BaoCaoNamBUS.GetData();
        }

        private void txtNam_TextChanged(object sender, EventArgs e)
        {
            if (txtNam.Text != "")
            {
                int id = int.Parse(txtNam.Text.Trim());

                List<BaoCaoNamDTO> lst = BaoCaoNamBUS.FindItem(id);
                if (lst != null)
                {
                    dgvNam.DataSource = lst;
                }
            }
        }
    }
}
