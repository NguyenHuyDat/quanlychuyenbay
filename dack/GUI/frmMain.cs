﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dack.DTO;
using dack.BUS;

namespace dack.GUI
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            LoadDGVPCB();

            tsmiSanBay.Click+=new EventHandler(tsmiChuyenBay_Click);

            tsmiGiaVe.Click += new EventHandler(tsmiGiaVe_Click);
            tmsi_HangVe.Click += new EventHandler(tmsi_HangVe_Click);

            tsmiExit.Click += new EventHandler(tsmiExit_Click);
            tsmiVeChuyenBay.Click += new EventHandler(tsmiVeChuyenBay_Click);
            tsmiThang.Click += new EventHandler(tsmiThang_Click);
            tsmiNam.Click += new EventHandler(tsmiNam_Click);

            btn_TV.Click += new EventHandler(btn_TV_Click);

            LoadCB();
            LoadHK();
            LoadHV();
        }

        void btn_TV_Click(object sender, EventArgs e)
        {
            if (cb_cb.SelectedIndex == -1) {
                MessageBox.Show("Chọn chuyến bay.");
            }
            else if (cb_hk.SelectedIndex == -1) {
                MessageBox.Show("Chọn hành khách.");
            }
            else if (cb_hv.SelectedIndex == -1)
            {
                MessageBox.Show("Chọn hạng vé.");
            }
            else {
                int mhv = (cb_hv.SelectedItem as dynamic).Value;
                int mcb = (cb_cb.SelectedItem as dynamic).Value;
                int mhk = (cb_hk.SelectedItem as dynamic).Value;

                int gt = 0;

                ChuyenBayDTO cb = ChuyenBayBUS.GetChuyenBayByID(mcb);
                SanBayDTO sbdi = SanBayBUS.GetSanBayByID(cb.san_bay_di);
                SanBayDTO sbden = SanBayBUS.GetSanBayByID(cb.san_bay_den);

                GiaVeDTO gvdi = GiaVeBUS.GetGiaVeByID(sbdi.ma_san_bay, mhv);
                GiaVeDTO gvden = GiaVeBUS.GetGiaVeByID(sbden.ma_san_bay, mhv);

                gt = gvdi.GV_Don_Gia + gvden.GV_Don_Gia;
                txtGT.Text = gt.ToString();

                VeChuyenBayDTO vcb = new VeChuyenBayDTO()
                {
                    ma_hang_ve = mhv,
                    ma_chuyen_bay = mcb,
                    ma_hanh_khach = mhk,
                    gia_tien = gt
                };

                DbAck ack = VeChuyenBayBUS.Insert(vcb);
                if (ack == DbAck.Ok)
                {
                    MessageBox.Show("Thêm thành công");
                    LoadDGVPCB();
                }
                else
                {
                    MessageBox.Show("Có lỗi.");
                }
            }
        }

        void LoadDGVPCB() {
            List<VeChuyenBayDTO> lstCB = VeChuyenBayBUS.SelectAllVeChuyenBay();
            if (lstCB != null)
            {
                dgvVeCB.DataSource = lstCB;
            }
        }

        void LoadCB() {
            List<ChuyenBayDTO> lstCB = ChuyenBayBUS.SelectAllChuyenBay();
            if (lstCB != null) {
                cb_cb.DisplayMember = "Text";
                cb_cb.ValueMember = "Value";

                foreach (var item in lstCB) {
                    cb_cb.Items.Add(new { Text = item.ma_chuyen_bay, Value = item.ma_chuyen_bay});
                }
            }
        }

        void LoadHK() {
            List<HanhKhachDTO> lstHK = HanhKhachBUS.SelectAllHanhKhach();
            if (lstHK != null)
            {
                cb_hk.DisplayMember = "Text";
                cb_hk.ValueMember = "Value";

                foreach (var item in lstHK)
                {
                    cb_hk.Items.Add(new { Text = item.ten_hanh_khach, Value = item.ma_hanh_khach });
                }
            }
        }

        void LoadHV()
        {
            List<HangVeDTO> lstHV = HangVeBUS.SelectAllHangVe();
            if (lstHV != null)
            {
                cb_hv.DisplayMember = "Text";
                cb_hv.ValueMember = "Value";

                foreach (var item in lstHV)
                {
                    cb_hv.Items.Add(new { Text = item.loai_hang_ve, Value = item.ma_hang_ve });
                }
            }
        }

        void tsmiNam_Click(object sender, EventArgs e)
        {
            frmBaoCaoNam frm = new frmBaoCaoNam();
            frm.Show();
        }

        void tsmiThang_Click(object sender, EventArgs e)
        {
            frmBaoCaoThang frm = new frmBaoCaoThang();
            frm.Show();
        }

        void tsmiVeChuyenBay_Click(object sender, EventArgs e)
        {
            VeChuyenBay frm = new VeChuyenBay();
            frm.Show();
        }

        void tsmiExit_Click(object sender, EventArgs e)
        {
            this.Close();   
        }

        void tmsi_HangVe_Click(object sender, EventArgs e)
        {
            HangVe frm = new HangVe();
            frm.Show();
        }

        void tsmiGiaVe_Click(object sender, EventArgs e)
        {
            GiaVe frm = new GiaVe();
            frm.Show();
        }

        void tsmiChuyenBay_Click(object sender, EventArgs e)
        {
            SanBay sb = new SanBay();
            sb.Show();
        }

        private void hànhKháchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form hanhKhach = new HanhKhach();
            hanhKhach.Show();
        }

        private void thamSốToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form thamso = new ThamSo();
            thamso.Show();
        }

        private void tsmiChuyenBay_Click_1(object sender, EventArgs e)
        {
            frmChuyenBay cb = new frmChuyenBay();
            cb.Show();
        }
    }
}
