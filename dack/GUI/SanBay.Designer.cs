﻿namespace dack.GUI
{
    partial class SanBay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaSanBay = new System.Windows.Forms.TextBox();
            this.txtTenSanBay = new System.Windows.Forms.TextBox();
            this.dgvSanBay = new System.Windows.Forms.DataGridView();
            this.MaSanBay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenSanBay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSanBay)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã sân bay:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên sân bay:";
            // 
            // txtMaSanBay
            // 
            this.txtMaSanBay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMaSanBay.Enabled = false;
            this.txtMaSanBay.Location = new System.Drawing.Point(83, 24);
            this.txtMaSanBay.Name = "txtMaSanBay";
            this.txtMaSanBay.Size = new System.Drawing.Size(276, 20);
            this.txtMaSanBay.TabIndex = 1;
            // 
            // txtTenSanBay
            // 
            this.txtTenSanBay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTenSanBay.Location = new System.Drawing.Point(83, 51);
            this.txtTenSanBay.Name = "txtTenSanBay";
            this.txtTenSanBay.Size = new System.Drawing.Size(276, 20);
            this.txtTenSanBay.TabIndex = 2;
            // 
            // dgvSanBay
            // 
            this.dgvSanBay.AllowUserToAddRows = false;
            this.dgvSanBay.AllowUserToDeleteRows = false;
            this.dgvSanBay.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSanBay.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvSanBay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSanBay.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaSanBay,
            this.TenSanBay});
            this.dgvSanBay.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvSanBay.EnableHeadersVisualStyles = false;
            this.dgvSanBay.Location = new System.Drawing.Point(0, 137);
            this.dgvSanBay.MultiSelect = false;
            this.dgvSanBay.Name = "dgvSanBay";
            this.dgvSanBay.ReadOnly = true;
            this.dgvSanBay.RowHeadersVisible = false;
            this.dgvSanBay.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvSanBay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSanBay.Size = new System.Drawing.Size(374, 204);
            this.dgvSanBay.TabIndex = 6;
            // 
            // MaSanBay
            // 
            this.MaSanBay.DataPropertyName = "ma_san_bay";
            this.MaSanBay.FillWeight = 40F;
            this.MaSanBay.HeaderText = "Mã sân bay";
            this.MaSanBay.Name = "MaSanBay";
            this.MaSanBay.ReadOnly = true;
            // 
            // TenSanBay
            // 
            this.TenSanBay.DataPropertyName = "ten_san_bay";
            this.TenSanBay.HeaderText = "Tên sân bay";
            this.TenSanBay.Name = "TenSanBay";
            this.TenSanBay.ReadOnly = true;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(122, 92);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 3;
            this.btnAdd.Text = "Thêm";
            this.btnAdd.UseVisualStyleBackColor = true;
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(203, 92);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(75, 23);
            this.btnDel.TabIndex = 4;
            this.btnDel.Text = "Xóa";
            this.btnDel.UseVisualStyleBackColor = true;
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(284, 92);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 5;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.UseVisualStyleBackColor = true;
            // 
            // SanBay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 341);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.dgvSanBay);
            this.Controls.Add(this.txtTenSanBay);
            this.Controls.Add(this.txtMaSanBay);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "SanBay";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SanBay";
            this.Load += new System.EventHandler(this.SanBay_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSanBay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtMaSanBay;
        private System.Windows.Forms.TextBox txtTenSanBay;
        private System.Windows.Forms.DataGridView dgvSanBay;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaSanBay;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenSanBay;
    }
}