﻿namespace dack.GUI
{
    partial class GiaVe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cb_TenSanBay = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cb_HangVe = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_DonGia = new System.Windows.Forms.TextBox();
            this.btn_Them = new System.Windows.Forms.Button();
            this.btn_delete = new System.Windows.Forms.Button();
            this.btn_update = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvDSGSB = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDSGSB)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên Sân Bay";
            // 
            // cb_TenSanBay
            // 
            this.cb_TenSanBay.FormattingEnabled = true;
            this.cb_TenSanBay.Location = new System.Drawing.Point(172, 12);
            this.cb_TenSanBay.Name = "cb_TenSanBay";
            this.cb_TenSanBay.Size = new System.Drawing.Size(142, 21);
            this.cb_TenSanBay.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(81, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Hạng Vé";
            // 
            // cb_HangVe
            // 
            this.cb_HangVe.FormattingEnabled = true;
            this.cb_HangVe.Location = new System.Drawing.Point(172, 39);
            this.cb_HangVe.Name = "cb_HangVe";
            this.cb_HangVe.Size = new System.Drawing.Size(142, 21);
            this.cb_HangVe.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Đơn Giá";
            // 
            // txt_DonGia
            // 
            this.txt_DonGia.Location = new System.Drawing.Point(172, 66);
            this.txt_DonGia.Name = "txt_DonGia";
            this.txt_DonGia.Size = new System.Drawing.Size(142, 20);
            this.txt_DonGia.TabIndex = 5;
            // 
            // btn_Them
            // 
            this.btn_Them.Location = new System.Drawing.Point(370, 13);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(75, 23);
            this.btn_Them.TabIndex = 6;
            this.btn_Them.Text = "Thêm";
            this.btn_Them.UseVisualStyleBackColor = true;
            // 
            // btn_delete
            // 
            this.btn_delete.Location = new System.Drawing.Point(370, 39);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(75, 23);
            this.btn_delete.TabIndex = 7;
            this.btn_delete.Text = "Xóa";
            this.btn_delete.UseVisualStyleBackColor = true;
            // 
            // btn_update
            // 
            this.btn_update.Location = new System.Drawing.Point(370, 66);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(75, 23);
            this.btn_update.TabIndex = 8;
            this.btn_update.Text = "Sửa";
            this.btn_update.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvDSGSB);
            this.groupBox1.Location = new System.Drawing.Point(12, 95);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(712, 374);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Danh sách giá sân bay";
            // 
            // dgvDSGSB
            // 
            this.dgvDSGSB.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.dgvDSGSB.AllowUserToAddRows = false;
            this.dgvDSGSB.AllowUserToDeleteRows = false;
            this.dgvDSGSB.AllowUserToOrderColumns = true;
            this.dgvDSGSB.AllowUserToResizeColumns = false;
            this.dgvDSGSB.AllowUserToResizeRows = false;
            this.dgvDSGSB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDSGSB.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvDSGSB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDSGSB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDSGSB.Location = new System.Drawing.Point(3, 16);
            this.dgvDSGSB.MultiSelect = false;
            this.dgvDSGSB.Name = "dgvDSGSB";
            this.dgvDSGSB.ReadOnly = true;
            this.dgvDSGSB.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvDSGSB.RowHeadersVisible = false;
            this.dgvDSGSB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDSGSB.Size = new System.Drawing.Size(706, 355);
            this.dgvDSGSB.TabIndex = 0;
            // 
            // GiaVe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 481);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_update);
            this.Controls.Add(this.btn_delete);
            this.Controls.Add(this.btn_Them);
            this.Controls.Add(this.txt_DonGia);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cb_HangVe);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cb_TenSanBay);
            this.Controls.Add(this.label1);
            this.Name = "GiaVe";
            this.Text = "GiaVe";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDSGSB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_TenSanBay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cb_HangVe;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_DonGia;
        private System.Windows.Forms.Button btn_Them;
        private System.Windows.Forms.Button btn_delete;
        private System.Windows.Forms.Button btn_update;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvDSGSB;
    }
}