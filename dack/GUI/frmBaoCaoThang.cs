﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dack.BUS;
using dack.DTO;

namespace dack.GUI
{
    public partial class frmBaoCaoThang : Form
    {
        public frmBaoCaoThang()
        {
            InitializeComponent();
        }

        private void frmBaoCaoThang_Load(object sender, EventArgs e)
        {
            dgvBaoCao.DataSource = BaoCaoThangBUS.GetData();
        }

        private void txtThang_TextChanged(object sender, EventArgs e)
        {
            if (txtThang.Text != "")
            {
                int id = int.Parse(txtThang.Text.Trim());

                List<BaoCaoThangDTO> lst = BaoCaoThangBUS.FindItem(id);
                if (lst != null)
                {
                    dgvBaoCao.DataSource = lst;
                }
            }
        }
    }
}
