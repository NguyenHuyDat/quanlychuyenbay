﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dack.BUS;
using dack.DTO;

namespace dack.GUI
{
    public partial class GiaVe : Form
    {
        public GiaVe()
        {
            InitializeComponent();

            List<SanBayDTO> ListSanBay = null;

            ListSanBay = SanBayBUS.SelectAllSanBay();
            if (ListSanBay != null)
            {
                cb_TenSanBay.DisplayMember = "Text";
                cb_TenSanBay.ValueMember = "Value";

                foreach (var item in ListSanBay)
                {
                    cb_TenSanBay.Items.Add(new { Text = item.ten_san_bay, Value = item.ma_san_bay});
                }
            }


            List<HangVeDTO> ListHangVe = null;
            ListHangVe = HangVeBUS.SelectAllHangVe();

            if (ListHangVe != null) {
                cb_HangVe.DisplayMember = "Text";
                cb_HangVe.ValueMember = "Value";

                foreach (var item in ListHangVe)
                {
                    cb_HangVe.Items.Add(new { Text = item.loai_hang_ve, Value = item.ma_hang_ve });
                }
            }

            dgvLoad();


// Xử lý truyền thông tin từ dgv lên các textbox
            dgvDSGSB.SelectionChanged += new EventHandler(dgvDSGSB_SelectionChanged);

            btn_Them.Click += new EventHandler(btn_Them_Click);
            btn_update.Click += new EventHandler(btn_update_Click);
            btn_delete.Click += new EventHandler(btn_delete_Click);
        }

        void btn_delete_Click(object sender, EventArgs e)
        {
            if (cb_TenSanBay.SelectedIndex == -1)
            {
                MessageBox.Show("Moi ban Chon Ten San Bay");
            }
            else if (cb_HangVe.SelectedIndex == -1)
            {
                MessageBox.Show("Moi ban Chon Hang Ve");
            }
            else if (txt_DonGia.Text == "")
            {
                MessageBox.Show("Moi ban Nhap Don Gia");
            }
            else if (txt_DonGia.Text != "")
            {
                int n;
                bool isNumeric = int.TryParse(txt_DonGia.Text, out n);
                if (isNumeric == false)
                {
                    MessageBox.Show("Moi ban Nhap Don Gia bang so.");
                }
                else
                {
                    GiaVeDTO gv = new GiaVeDTO();
                    if (dgvDSGSB.SelectedRows.Count > 0)
                    {
                        gv.GV_ma_san_bay = int.Parse(dgvDSGSB.SelectedRows[0].Cells[0].Value.ToString());
                        gv.GV_ma_hang_ve = int.Parse(dgvDSGSB.SelectedRows[0].Cells[1].Value.ToString());
                        DbAck ack = GiaVeBUS.Delete(gv);
                        if (ack == DbAck.Ok)
                        {
                            MessageBox.Show("Thanh cong");
                            dgvLoad();
                        }
                        else
                        {
                            MessageBox.Show("Ko Thanh cong");
                        }
                    }
                }
            }
        }

        void dgvDSGSB_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvDSGSB.SelectedRows.Count > 0)
            {
                cb_TenSanBay.SelectedIndex = int.Parse(dgvDSGSB.SelectedRows[0].Cells[0].Value.ToString());
                cb_HangVe.Text = dgvDSGSB.SelectedRows[0].Cells[1].Value.ToString();
                txt_DonGia.Text = dgvDSGSB.SelectedRows[0].Cells[2].Value.ToString();
                //MessageBox.Show(dgvDSGSB.SelectedRows[0].Cells[1].Value.ToString());
            }
        }

        void dgvLoad() {
            dgvDSGSB.DataSource = GiaVeBUS.SelectAllGiaVe();
            dgvDSGSB.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        void btn_update_Click(object sender, EventArgs e)
        {
            if (cb_TenSanBay.SelectedIndex == -1)
            {
                MessageBox.Show("Moi ban Chon Ten San Bay");
            }
            else if (cb_HangVe.SelectedIndex == -1)
            {
                MessageBox.Show("Moi ban Chon Hang Ve");
            }
            else if (txt_DonGia.Text == "")
            {
                MessageBox.Show("Moi ban Nhap Don Gia");
            }
            else if (txt_DonGia.Text != "")
            {
                int n;
                bool isNumeric = int.TryParse(txt_DonGia.Text, out n);
                if (isNumeric == false)
                {
                    MessageBox.Show("Moi ban Nhap Don Gia bang so.");
                }
                else
                {
                    GiaVeDTO gv = new GiaVeDTO();
                    if (dgvDSGSB.SelectedRows.Count > 0)
                    {
                        gv.GV_ma_san_bay = int.Parse(dgvDSGSB.SelectedRows[0].Cells[0].Value.ToString());
                        gv.GV_ma_hang_ve = int.Parse(dgvDSGSB.SelectedRows[0].Cells[1].Value.ToString());
                        gv.GV_Don_Gia = int.Parse(txt_DonGia.Text);
                        DbAck ack = GiaVeBUS.Update(gv);
                        if (ack == DbAck.Ok)
                        {
                            MessageBox.Show("Thanh cong");
                            dgvLoad();
                        }
                        else
                        {
                            MessageBox.Show("Ko Thanh cong");
                        }
                    }
                    //int masb = (cb_TenSanBay.SelectedItem as dynamic).Value;
                    ////MessageBox.Show(masb.ToString());
                    //int mahv = int.Parse(cb_HangVe.Text);
                    ////MessageBox.Show(mahv.ToString());
                    //int dg = int.Parse(txt_DonGia.Text);
                    ////MessageBox.Show(dg.ToString());
                    //gv.GV_ma_san_bay = masb;
                    //gv.GV_ma_hang_ve = mahv;
                    //gv.GV_Don_Gia = dg;
                }
            }
        }

        void btn_Them_Click(object sender, EventArgs e)
        {

            if (cb_TenSanBay.SelectedIndex == -1)
            {
                MessageBox.Show("Moi ban Chon Ten San Bay");
            }
            else if (cb_HangVe.SelectedIndex == -1)
            {
                MessageBox.Show("Moi ban Chon Hang Ve");
            }
            else if (txt_DonGia.Text == "")
            {
                MessageBox.Show("Moi ban Nhap Don Gia");
            }
            else if (txt_DonGia.Text != "") {
                int n;
                bool isNumeric = int.TryParse(txt_DonGia.Text, out n);
                if (isNumeric == false) {
                    MessageBox.Show("Moi ban Nhap Don Gia bang so.");
                }
                else
                {
                    int masb = (cb_TenSanBay.SelectedItem as dynamic).Value;
                    //MessageBox.Show(masb.ToString());

                    int mahv = (cb_HangVe.SelectedItem as dynamic).Value;
                    //MessageBox.Show(mahv.ToString());
                    int dg = int.Parse(txt_DonGia.Text);

                    GiaVeDTO gv = new GiaVeDTO();

                    gv.GV_ma_san_bay = masb;
                    gv.GV_ma_hang_ve = mahv;
                    gv.GV_Don_Gia = dg;
                    DbAck ack = GiaVeBUS.Insert(gv);
                    if (ack == DbAck.Ok)
                    {
                        MessageBox.Show("Thanh cong");
                        dgvLoad();
                    }
                    else
                    {
                        MessageBox.Show("Ko Thanh cong");
                    }
                }
            }
        }

    }

}
