﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dack.DTO;
using dack.BUS;

namespace dack.GUI
{
    public partial class ThamSo : Form
    {
        public ThamSo()
        {
            InitializeComponent();
        }

        private void ThamSo_Load(object sender, EventArgs e)
        {
            ThamSo_Load();
            btnSua.Click +=new EventHandler(btnSua_Click);
        }

        private void ThamSo_Load()
        {
            ThamSoDTO ts = null;
            ts = ThamSoBUS.SelectAllThamSo();

            txtThoiGianBayToiThieu.Text = ts.thoi_gian_bay_toi_thieu.ToString();
            txtThoiGianDungToiDa.Text = ts.thoi_gian_dung_toi_da.ToString();
            txtThoiGianDungToiThieu.Text = ts.thoi_gian_dung_toi_thieu.ToString();
            txtSoLuongSanBayTrungGianToiDa.Text = ts.so_luong_san_bay_trung_gian_toi_da.ToString();
            txtNgayDatVeToiThieu.Text = ts.ngay_dat_ve_toi_thieu.ToString();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            int ngayDatVeToiThieu = Int32.Parse(txtNgayDatVeToiThieu.Text);
            int soLuongSanBayTrungGianToiDa = int.Parse(txtSoLuongSanBayTrungGianToiDa.Text);
            int thoiGianDungToiThieu = int.Parse(txtThoiGianDungToiThieu.Text);
            int thoiGianDungToiDa = int.Parse(txtThoiGianDungToiDa.Text);
            float thoiGianBayToiTHieu = float.Parse(txtThoiGianBayToiThieu.Text);

            if (ngayDatVeToiThieu.ToString() == "" || soLuongSanBayTrungGianToiDa.ToString() == "" || thoiGianDungToiThieu.ToString() == "" || thoiGianDungToiDa.ToString() == "" || thoiGianBayToiTHieu.ToString() == "")
            {
                MessageBox.Show("Nhập Thiếu Dữ Liệu.");
            }
            else
            {
                ThamSoDTO thamso = null;
                thamso = new ThamSoDTO()
                {
                    ngay_dat_ve_toi_thieu = ngayDatVeToiThieu,
                    so_luong_san_bay_trung_gian_toi_da = soLuongSanBayTrungGianToiDa,
                    thoi_gian_dung_toi_thieu = thoiGianDungToiThieu,
                    thoi_gian_dung_toi_da = thoiGianDungToiDa,
                    thoi_gian_bay_toi_thieu = thoiGianBayToiTHieu

                };

                DbAck ack = ThamSoBUS.Update(thamso);
                if (ack == DbAck.Ok)
                {
                    ThamSo_Load();
                }
                else
                {
                    if (ack == DbAck.NetworkError)
                    {
                        MessageBox.Show("Mạng không ổn định.", "Thông báo");
                    }
                    else if (ack == DbAck.Unknow)
                    {
                        MessageBox.Show("Unknow", "Notice");

                    }
                }
            }
        }





    }
}
