﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dack.DAO;
using dack.DTO;
using dack.BUS;

namespace dack.GUI
{
    public partial class SanBay : Form
    {
        public SanBay()
        {
            InitializeComponent();
        }

        private void SanBay_Load(object sender, EventArgs e)
        {
            LoadListSanBay();

            btnAdd.Click += new EventHandler(btnAdd_Click);
            dgvSanBay.SelectionChanged += new EventHandler(dgvSanBay_SelectionChanged);

            btnDel.Click += new EventHandler(btnDel_Click);
            btnEdit.Click += new EventHandler(btnEdit_Click);
        }

        void btnEdit_Click(object sender, EventArgs e)
        {
            if (txtTenSanBay.Text == "")
            {
                MessageBox.Show("Nhập tên sân bay.");
            }
            else if (txtMaSanBay.Text == "")
            {
                MessageBox.Show("Chọn sân bay.");
            }
            else
            {
                List<SanBayDTO> listSB = new List<SanBayDTO>();
                listSB = SanBayBUS.SelectAllSanBay();

                if (listSB != null)
                {
                    foreach (var item in listSB)
                    {
                        if (item.ten_san_bay.Equals(txtTenSanBay.Text))
                        {
                            MessageBox.Show("Tên sân bay đã tồn tại.");
                            return;
                        }
                    }

                    SanBayDTO sb = new SanBayDTO()
                    {
                        ma_san_bay = int.Parse(txtMaSanBay.Text),
                        ten_san_bay = txtTenSanBay.Text,
                    };
                    DbAck ack = SanBayBUS.Update(sb);

                    if (ack == DbAck.Ok)
                    {
                        MessageBox.Show("Sửa thành công.");
                        LoadListSanBay();
                    }
                    else
                    {
                        MessageBox.Show("Có lỗi xảy ra.");
                    }
                }
            }
        }

        void btnDel_Click(object sender, EventArgs e)
        {
            if (dgvSanBay.SelectedRows.Count > 0) {
                int maSanBay = int.Parse(dgvSanBay.SelectedRows[0].Cells[0].Value.ToString());
                SanBayDTO sb = new SanBayDTO { 
                    ma_san_bay = maSanBay
                };
                DbAck ack = SanBayBUS.Delete(sb);

                if (ack == DbAck.Ok)
                {
                    MessageBox.Show("Xóa thành công");
                    LoadListSanBay();
                }
                else {
                    MessageBox.Show("Có lỗi xảy ra");
                }
            }
        }

        void dgvSanBay_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvSanBay.SelectedRows.Count > 0) {
                txtMaSanBay.Text = dgvSanBay.SelectedRows[0].Cells[0].Value.ToString();
                txtTenSanBay.Text = dgvSanBay.SelectedRows[0].Cells[1].Value.ToString();
            }
        }

        void btnAdd_Click(object sender, EventArgs e)
        {
            string TenSanBay = txtTenSanBay.Text;
            if (TenSanBay == "")
            {
                MessageBox.Show("Nhập tên sân bay.");
            }
            else {
                SanBayDTO sanbay = null;
                sanbay = new SanBayDTO() { 
                    ten_san_bay = TenSanBay
                };

                DbAck ack = SanBayBUS.Insert(sanbay);
                if (ack == DbAck.Ok)
                {
                    MessageBox.Show("Thêm thành công.", "Thông báo");
                    LoadListSanBay();
                }
                else {
                    if (ack == DbAck.NetworkError) {
                        MessageBox.Show("Mạng không ổn định.", "Thông báo");
                    }
                    else if (ack == DbAck.Unknow) {
                        MessageBox.Show("Unknow", "Notice");
                    }
                }
            }
        }

        void LoadListSanBay() {
            List<SanBayDTO> listSanBay = SanBayBUS.SelectAllSanBay();

            // Load DGV
            dgvSanBay.DataSource = listSanBay;
        }
    }
}
