﻿namespace dack.GUI
{
    partial class ThamSo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtThoiGianBayToiThieu = new System.Windows.Forms.TextBox();
            this.txtThoiGianDungToiThieu = new System.Windows.Forms.TextBox();
            this.txtSoLuongSanBayTrungGianToiDa = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSua = new System.Windows.Forms.Button();
            this.txtThoiGianDungToiDa = new System.Windows.Forms.TextBox();
            this.txtNgayDatVeToiThieu = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtThoiGianBayToiThieu
            // 
            this.txtThoiGianBayToiThieu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtThoiGianBayToiThieu.Location = new System.Drawing.Point(239, 126);
            this.txtThoiGianBayToiThieu.Name = "txtThoiGianBayToiThieu";
            this.txtThoiGianBayToiThieu.Size = new System.Drawing.Size(120, 20);
            this.txtThoiGianBayToiThieu.TabIndex = 42;
            // 
            // txtThoiGianDungToiThieu
            // 
            this.txtThoiGianDungToiThieu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtThoiGianDungToiThieu.Location = new System.Drawing.Point(239, 53);
            this.txtThoiGianDungToiThieu.Name = "txtThoiGianDungToiThieu";
            this.txtThoiGianDungToiThieu.Size = new System.Drawing.Size(120, 20);
            this.txtThoiGianDungToiThieu.TabIndex = 41;
            // 
            // txtSoLuongSanBayTrungGianToiDa
            // 
            this.txtSoLuongSanBayTrungGianToiDa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSoLuongSanBayTrungGianToiDa.Location = new System.Drawing.Point(239, 165);
            this.txtSoLuongSanBayTrungGianToiDa.Name = "txtSoLuongSanBayTrungGianToiDa";
            this.txtSoLuongSanBayTrungGianToiDa.Size = new System.Drawing.Size(120, 20);
            this.txtSoLuongSanBayTrungGianToiDa.TabIndex = 40;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(29, 21);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(119, 13);
            this.label5.TabIndex = 39;
            this.label5.Text = "Ngày Đặt Vé Tối Thiểu:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(29, 172);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(187, 13);
            this.label4.TabIndex = 38;
            this.label4.Text = "Số Lượng Sân Bay Trung Gian Tối Đa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(29, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(133, 13);
            this.label3.TabIndex = 37;
            this.label3.Text = "Thời Gian Dừng Tối Thiểu:";
            // 
            // btnSua
            // 
            this.btnSua.Location = new System.Drawing.Point(169, 217);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(71, 23);
            this.btnSua.TabIndex = 36;
            this.btnSua.Text = "Sửa";
            this.btnSua.UseVisualStyleBackColor = true;
            // 
            // txtThoiGianDungToiDa
            // 
            this.txtThoiGianDungToiDa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtThoiGianDungToiDa.Location = new System.Drawing.Point(239, 91);
            this.txtThoiGianDungToiDa.Name = "txtThoiGianDungToiDa";
            this.txtThoiGianDungToiDa.Size = new System.Drawing.Size(120, 20);
            this.txtThoiGianDungToiDa.TabIndex = 35;
            // 
            // txtNgayDatVeToiThieu
            // 
            this.txtNgayDatVeToiThieu.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNgayDatVeToiThieu.Location = new System.Drawing.Point(239, 18);
            this.txtNgayDatVeToiThieu.Name = "txtNgayDatVeToiThieu";
            this.txtNgayDatVeToiThieu.Size = new System.Drawing.Size(120, 20);
            this.txtNgayDatVeToiThieu.TabIndex = 34;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(29, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Thời Gian Dừng Tối Đa:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 129);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Thời Gian Bay Tối Thiểu:";
            // 
            // ThamSo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 267);
            this.Controls.Add(this.txtThoiGianBayToiThieu);
            this.Controls.Add(this.txtThoiGianDungToiThieu);
            this.Controls.Add(this.txtSoLuongSanBayTrungGianToiDa);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnSua);
            this.Controls.Add(this.txtThoiGianDungToiDa);
            this.Controls.Add(this.txtNgayDatVeToiThieu);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ThamSo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ThamSo";
            this.Load += new System.EventHandler(this.ThamSo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtThoiGianBayToiThieu;
        private System.Windows.Forms.TextBox txtThoiGianDungToiThieu;
        private System.Windows.Forms.TextBox txtSoLuongSanBayTrungGianToiDa;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnSua;
        private System.Windows.Forms.TextBox txtThoiGianDungToiDa;
        private System.Windows.Forms.TextBox txtNgayDatVeToiThieu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}