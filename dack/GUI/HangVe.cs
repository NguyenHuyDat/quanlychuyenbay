﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dack.DTO;
using dack.BUS;

namespace dack.GUI
{
    public partial class HangVe : Form
    {
        public HangVe()
        {
            InitializeComponent();

            btn_Them.Click += new EventHandler(btn_Them_Click);
            btn_Update.Click += new EventHandler(btn_Update_Click);
            btn_delete.Click += new EventHandler(btn_delete_Click);
            DGVLoaiHangve.SelectionChanged += new EventHandler(DGVLoaiHangve_SelectionChanged);
            dgvLoad();
        }

        void btn_delete_Click(object sender, EventArgs e)
        {
            if (txt_LoaiHangVe.Text == "")
            {
                MessageBox.Show("Moi ban Nhap Loai Hang Ve");
            }
            else if (txt_LoaiHangVe.Text != "")
            {
                int n;
                bool isNumeric = int.TryParse(txt_LoaiHangVe.Text, out n);
                if (isNumeric == false)
                {
                    MessageBox.Show("Moi ban Nhap Hang Ve bang so");
                }
                else
                {
                    HangVeDTO hv = new HangVeDTO();
                    if (DGVLoaiHangve.SelectedRows.Count > 0)
                    {
                        hv.ma_hang_ve = int.Parse(DGVLoaiHangve.SelectedRows[0].Cells[0].Value.ToString());
                        
                        DbAck ack = HangVeBUS.Delete(hv);
                        if (ack == DbAck.Ok)
                        {
                            MessageBox.Show("Thanh cong");
                            dgvLoad();
                        }
                        else
                        {
                            MessageBox.Show("Ko Thanh cong");
                        }
                    }
                }
            }
        }

        void DGVLoaiHangve_SelectionChanged(object sender, EventArgs e)
        {
            if (DGVLoaiHangve.SelectedRows.Count > 0)
            {
                int ma_hang_ve = int.Parse(DGVLoaiHangve.SelectedRows[0].Cells[0].Value.ToString());
                txt_LoaiHangVe.Text = DGVLoaiHangve.SelectedRows[0].Cells[1].Value.ToString();
            }
        }

        void btn_Update_Click(object sender, EventArgs e)
        {
            if (txt_LoaiHangVe.Text == "")
            {
                MessageBox.Show("Moi ban Nhap Loai Hang Ve");
            }
            else if (txt_LoaiHangVe.Text != "")
            {
                int n;
                bool isNumeric = int.TryParse(txt_LoaiHangVe.Text, out n);
                if (isNumeric == false)
                {
                    MessageBox.Show("Moi ban Nhap Hang Ve bang so");
                }
                else
                {
                    HangVeDTO hv = new HangVeDTO();
                    if (DGVLoaiHangve.SelectedRows.Count > 0)
                    {
                        hv.ma_hang_ve = int.Parse(DGVLoaiHangve.SelectedRows[0].Cells[0].Value.ToString());
                        hv.loai_hang_ve = txt_LoaiHangVe.Text;
                        DbAck ack = HangVeBUS.Update(hv);
                        if (ack == DbAck.Ok)
                        {
                            MessageBox.Show("Thanh cong");
                            dgvLoad();
                        }
                        else
                        {
                            MessageBox.Show("Ko Thanh cong");
                        }
                    }
                }
            }
        }
        void dgvLoad()
        {
            DGVLoaiHangve.DataSource = HangVeBUS.SelectAllHangVe();
        }
        void btn_Them_Click(object sender, EventArgs e)
        {
            if (txt_LoaiHangVe.Text == "")
            {
                MessageBox.Show("Moi ban Nhap Loai Hang Ve");
            }
            else if (txt_LoaiHangVe.Text != "")
            {
                int n;
                bool isNumeric = int.TryParse(txt_LoaiHangVe.Text, out n);
                if (isNumeric == false)
                {
                    MessageBox.Show("Moi ban Nhap Hang Ve bang so");
                }
                else
                {
                    HangVeDTO hv = new HangVeDTO();
                    hv.loai_hang_ve = txt_LoaiHangVe.Text;

                    DbAck ack = HangVeBUS.Insert(hv);
                    if (ack == DbAck.Ok)
                    {
                        MessageBox.Show("Thanh cong");
                        dgvLoad();
                    }
                    else
                    {
                        MessageBox.Show("Ko Thanh cong");
                    }
                }
            }
        }
    }
}
