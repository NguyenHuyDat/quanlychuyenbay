﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dack.DAO;
using dack.DTO;
using dack.BUS;

namespace dack.GUI
{
    public partial class HanhKhach : Form
    {
        public HanhKhach()
        {
            InitializeComponent();
        }

        private void HanhKhach_Load(object sender, EventArgs e)
        {
            LoadListHanhKhach(); 
            dgvHanhKhach.SelectionChanged += new EventHandler(dgvHanhKhach_SelectionChanged);

        }

        private void LoadListHanhKhach()
        {
            List<HanhKhachDTO> listHanhKhach = HanhKhachBUS.SelectAllHanhKhach();
            // Load DGV
            dgvHanhKhach.DataSource = listHanhKhach;
        }

        void dgvHanhKhach_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvHanhKhach.SelectedRows.Count > 0)
            {
                txtMaHanhKhach.Text = dgvHanhKhach.SelectedRows[0].Cells[0].Value.ToString();
                txtNgayDat.Text = dgvHanhKhach.SelectedRows[0].Cells[1].Value.ToString();
                txtTenHanhKhach.Text = dgvHanhKhach.SelectedRows[0].Cells[2].Value.ToString();
                txtCMND.Text = dgvHanhKhach.SelectedRows[0].Cells[3].Value.ToString();
                txtSDT.Text = dgvHanhKhach.SelectedRows[0].Cells[4].Value.ToString();

            }
        }



        private void btnXoa_Click(object sender, EventArgs e)
        {
            int maKhachHang = Int32.Parse(txtMaHanhKhach.Text);
            HanhKhachDTO HanhKhach = null;
            HanhKhach = new HanhKhachDTO()
            {
                ma_hanh_khach = maKhachHang
            };
            DbAck ack = HanhKhachBUS.Delete(HanhKhach);
            if (ack == DbAck.Ok)
            {
                LoadListHanhKhach();
            }
            else
            {
                if (ack == DbAck.NetworkError)
                {
                    MessageBox.Show("Mạng không ổn định.", "Thông báo");
                }
                else if (ack == DbAck.Unknow)
                {
                    MessageBox.Show("Unknow", "Notice");

                }
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            int maKhachHang = Int32.Parse(txtMaHanhKhach.Text);
            DateTime ngayDat = DateTime.Parse(txtNgayDat.Text);
            string tenKhachHang = txtTenHanhKhach.Text;
            int cmnd = int.Parse(txtCMND.Text);
            int sdt = int.Parse(txtSDT.Text);
            //maKhachHang.ToString() == "" ||
            if (ngayDat.ToString() == "" || tenKhachHang.ToString() == "" || cmnd.ToString() == "" || sdt.ToString() == "")
            {
                MessageBox.Show("Nhập Thiếu Dữ Liệu.");
            }
            else
            {
                HanhKhachDTO HanhKhach = null;
                HanhKhach = new HanhKhachDTO()
                {
                    ma_hanh_khach = maKhachHang,
                    ngay_dat = ngayDat,
                    ten_hanh_khach = tenKhachHang,
                    cmnd = cmnd,
                    dien_thoai = sdt

                };

                DbAck ack = HanhKhachBUS.Update(HanhKhach);
                if (ack == DbAck.Ok)
                {
                    LoadListHanhKhach();
                }
                else
                {
                    if (ack == DbAck.NetworkError)
                    {
                        MessageBox.Show("Mạng không ổn định.", "Thông báo");
                    }
                    else if (ack == DbAck.Unknow)
                    {
                        MessageBox.Show("Unknow", "Notice");

                    }
                }
            }
        }

        private void btnThem_Click_1(object sender, EventArgs e)
        {
            Form themHanhKhach = new ThemHanhKhach();
            themHanhKhach.Show();
        }

    }
}
