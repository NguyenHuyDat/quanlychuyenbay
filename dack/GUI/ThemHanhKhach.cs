﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dack.DAO;
using dack.DTO;
using dack.BUS;

namespace dack.GUI
{
    public partial class ThemHanhKhach : Form
    {
        public ThemHanhKhach()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string tenHanhKhach = txtTenHanhKhach.Text;
            string ngayDat = dtpNgayDat.Value.ToShortDateString();
            int cmnd = int.Parse(txtCMND.Text);
            int dienThoai = int.Parse(txtSDT.Text);
            if (tenHanhKhach == "" || ngayDat == "" || cmnd.ToString() == "" || dienThoai.ToString() == "")
            {
                MessageBox.Show("Vui Lòng Nhập Đủ Thông Tin");
            }
            else
            {
                HanhKhachDTO HanhKhach = null;
                HanhKhach = new HanhKhachDTO()
                {
                    ten_hanh_khach = tenHanhKhach,
                    ngay_dat = Convert.ToDateTime(ngayDat),
                    cmnd = cmnd,
                    dien_thoai = dienThoai
                };

                DbAck ack = HanhKhachBUS.Insert(HanhKhach);
                if (ack == DbAck.Ok)
                {
                    MessageBox.Show("Thêm Thành Công");
                }
                else
                {
                    if (ack == DbAck.NetworkError)
                    {
                        MessageBox.Show("Mạng không ổn định.", "Thông báo");
                    }
                    else if (ack == DbAck.Unknow)
                    {
                        MessageBox.Show("Unknow", "Notice");
                    }
                }
            }
        }



    }
}
