﻿namespace dack.GUI
{
    partial class frmChuyenBay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtThoiGianBay = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSlghehang1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSlghehang2 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtslGhedadathang2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtslGhedadathang1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbSBDi = new System.Windows.Forms.ComboBox();
            this.cbSBDen = new System.Windows.Forms.ComboBox();
            this.dtpNgayGio = new System.Windows.Forms.DateTimePicker();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.cbSBTG = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTGD = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtGC = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtMaCB = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnAdd_sbtg = new System.Windows.Forms.Button();
            this.dgvDSCB = new System.Windows.Forms.DataGridView();
            this.MaSanBay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sanbaydi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvDSSBTG = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenSanBay = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDel_sbtg = new System.Windows.Forms.Button();
            this.btnEdit_sbtg = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDSCB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDSSBTG)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Sân bay đi:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Sân bay đến: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ngày giờ:";
            // 
            // txtThoiGianBay
            // 
            this.txtThoiGianBay.Location = new System.Drawing.Point(124, 122);
            this.txtThoiGianBay.Name = "txtThoiGianBay";
            this.txtThoiGianBay.Size = new System.Drawing.Size(164, 20);
            this.txtThoiGianBay.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Thời gian bay:";
            // 
            // txtSlghehang1
            // 
            this.txtSlghehang1.Location = new System.Drawing.Point(124, 151);
            this.txtSlghehang1.Name = "txtSlghehang1";
            this.txtSlghehang1.Size = new System.Drawing.Size(164, 20);
            this.txtSlghehang1.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 154);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(106, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Số lượng ghế hạng 1";
            // 
            // txtSlghehang2
            // 
            this.txtSlghehang2.Location = new System.Drawing.Point(124, 179);
            this.txtSlghehang2.Name = "txtSlghehang2";
            this.txtSlghehang2.Size = new System.Drawing.Size(164, 20);
            this.txtSlghehang2.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 182);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(106, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Số lượng ghế hạng 2";
            // 
            // txtslGhedadathang2
            // 
            this.txtslGhedadathang2.Enabled = false;
            this.txtslGhedadathang2.Location = new System.Drawing.Point(124, 234);
            this.txtslGhedadathang2.Name = "txtslGhedadathang2";
            this.txtslGhedadathang2.Size = new System.Drawing.Size(164, 20);
            this.txtslGhedadathang2.TabIndex = 18;
            this.txtslGhedadathang2.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 237);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Số ghế đã đặt hạng 2";
            // 
            // txtslGhedadathang1
            // 
            this.txtslGhedadathang1.Enabled = false;
            this.txtslGhedadathang1.Location = new System.Drawing.Point(124, 207);
            this.txtslGhedadathang1.Name = "txtslGhedadathang1";
            this.txtslGhedadathang1.Size = new System.Drawing.Size(164, 20);
            this.txtslGhedadathang1.TabIndex = 16;
            this.txtslGhedadathang1.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 210);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Số ghế đã đặt hạng 1";
            // 
            // cbSBDi
            // 
            this.cbSBDi.FormattingEnabled = true;
            this.cbSBDi.Location = new System.Drawing.Point(124, 33);
            this.cbSBDi.Name = "cbSBDi";
            this.cbSBDi.Size = new System.Drawing.Size(267, 21);
            this.cbSBDi.TabIndex = 19;
            // 
            // cbSBDen
            // 
            this.cbSBDen.FormattingEnabled = true;
            this.cbSBDen.Location = new System.Drawing.Point(124, 62);
            this.cbSBDen.Name = "cbSBDen";
            this.cbSBDen.Size = new System.Drawing.Size(267, 21);
            this.cbSBDen.TabIndex = 20;
            // 
            // dtpNgayGio
            // 
            this.dtpNgayGio.Location = new System.Drawing.Point(124, 92);
            this.dtpNgayGio.Name = "dtpNgayGio";
            this.dtpNgayGio.Size = new System.Drawing.Size(164, 20);
            this.dtpNgayGio.TabIndex = 21;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(831, 4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 23);
            this.btnAdd.TabIndex = 22;
            this.btnAdd.Text = "Thêm";
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(831, 31);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(75, 23);
            this.btnDel.TabIndex = 23;
            this.btnDel.Text = "Xóa";
            this.btnDel.UseVisualStyleBackColor = true;
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(831, 56);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 24;
            this.btnEdit.Text = "Sửa";
            this.btnEdit.UseVisualStyleBackColor = true;
            // 
            // cbSBTG
            // 
            this.cbSBTG.FormattingEnabled = true;
            this.cbSBTG.Location = new System.Drawing.Point(586, 6);
            this.cbSBTG.Name = "cbSBTG";
            this.cbSBTG.Size = new System.Drawing.Size(222, 21);
            this.cbSBTG.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(474, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Sân bay trung gian";
            // 
            // txtTGD
            // 
            this.txtTGD.Location = new System.Drawing.Point(585, 33);
            this.txtTGD.Name = "txtTGD";
            this.txtTGD.Size = new System.Drawing.Size(222, 20);
            this.txtTGD.TabIndex = 30;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(473, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 13);
            this.label11.TabIndex = 29;
            this.label11.Text = "Thời gian dừng";
            // 
            // txtGC
            // 
            this.txtGC.Location = new System.Drawing.Point(586, 62);
            this.txtGC.Multiline = true;
            this.txtGC.Name = "txtGC";
            this.txtGC.Size = new System.Drawing.Size(222, 50);
            this.txtGC.TabIndex = 32;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(475, 65);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 13);
            this.label12.TabIndex = 31;
            this.label12.Text = "Ghi chú";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(80, 13);
            this.label15.TabIndex = 39;
            this.label15.Text = "Mã chuyến bay";
            // 
            // txtMaCB
            // 
            this.txtMaCB.Enabled = false;
            this.txtMaCB.Location = new System.Drawing.Point(124, 6);
            this.txtMaCB.Name = "txtMaCB";
            this.txtMaCB.Size = new System.Drawing.Size(267, 20);
            this.txtMaCB.TabIndex = 40;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvDSCB);
            this.groupBox1.Location = new System.Drawing.Point(12, 274);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(900, 231);
            this.groupBox1.TabIndex = 41;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Danh sách chuyến bay";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgvDSSBTG);
            this.groupBox2.Location = new System.Drawing.Point(332, 122);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(577, 128);
            this.groupBox2.TabIndex = 42;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Danh sách sân bay trung gian";
            // 
            // btnAdd_sbtg
            // 
            this.btnAdd_sbtg.Location = new System.Drawing.Point(428, 253);
            this.btnAdd_sbtg.Name = "btnAdd_sbtg";
            this.btnAdd_sbtg.Size = new System.Drawing.Size(142, 23);
            this.btnAdd_sbtg.TabIndex = 43;
            this.btnAdd_sbtg.Text = "Thêm sân bay trung gian";
            this.btnAdd_sbtg.UseVisualStyleBackColor = true;
            // 
            // dgvDSCB
            // 
            this.dgvDSCB.AllowUserToAddRows = false;
            this.dgvDSCB.AllowUserToDeleteRows = false;
            this.dgvDSCB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDSCB.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvDSCB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDSCB.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaSanBay,
            this.sanbaydi,
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6,
            this.Column7});
            this.dgvDSCB.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgvDSCB.EnableHeadersVisualStyles = false;
            this.dgvDSCB.Location = new System.Drawing.Point(3, 24);
            this.dgvDSCB.MultiSelect = false;
            this.dgvDSCB.Name = "dgvDSCB";
            this.dgvDSCB.ReadOnly = true;
            this.dgvDSCB.RowHeadersVisible = false;
            this.dgvDSCB.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvDSCB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDSCB.Size = new System.Drawing.Size(894, 204);
            this.dgvDSCB.TabIndex = 7;
            // 
            // MaSanBay
            // 
            this.MaSanBay.DataPropertyName = "ma_chuyen_bay";
            this.MaSanBay.FillWeight = 103.3136F;
            this.MaSanBay.HeaderText = "Mã chuyến bay";
            this.MaSanBay.Name = "MaSanBay";
            this.MaSanBay.ReadOnly = true;
            // 
            // sanbaydi
            // 
            this.sanbaydi.DataPropertyName = "san_bay_di";
            this.sanbaydi.FillWeight = 143.7781F;
            this.sanbaydi.HeaderText = "Sân bay đi";
            this.sanbaydi.Name = "sanbaydi";
            this.sanbaydi.ReadOnly = true;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "san_bay_den";
            this.Column1.FillWeight = 143.7781F;
            this.Column1.HeaderText = "Sân bay đến";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "ngay_gio";
            this.Column2.FillWeight = 143.7781F;
            this.Column2.HeaderText = "Ngày giờ";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "thoi_gian_bay";
            this.Column3.FillWeight = 143.7781F;
            this.Column3.HeaderText = "Thời gian bay";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "so_ghe_hang1";
            this.Column4.FillWeight = 55.3934F;
            this.Column4.HeaderText = "Số ghế hạng 1";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Visible = false;
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "so_ghe_hang2";
            this.Column5.FillWeight = 55.3934F;
            this.Column5.HeaderText = "Số ghế hạng 2";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Visible = false;
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "so_ghe_da_dat_hang1";
            this.Column6.FillWeight = 55.3934F;
            this.Column6.HeaderText = "Số lượng ghế đã đặt hạng 1";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Visible = false;
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "so_ghe_da_dat_hang2";
            this.Column7.FillWeight = 55.3934F;
            this.Column7.HeaderText = "Số lượng ghế đã đặt hạng 2";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Visible = false;
            // 
            // dgvDSSBTG
            // 
            this.dgvDSSBTG.AllowUserToAddRows = false;
            this.dgvDSSBTG.AllowUserToDeleteRows = false;
            this.dgvDSSBTG.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDSSBTG.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvDSSBTG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDSSBTG.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.TenSanBay,
            this.Column8,
            this.Column9});
            this.dgvDSSBTG.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvDSSBTG.EnableHeadersVisualStyles = false;
            this.dgvDSSBTG.Location = new System.Drawing.Point(3, 16);
            this.dgvDSSBTG.MultiSelect = false;
            this.dgvDSSBTG.Name = "dgvDSSBTG";
            this.dgvDSSBTG.ReadOnly = true;
            this.dgvDSSBTG.RowHeadersVisible = false;
            this.dgvDSSBTG.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgvDSSBTG.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDSSBTG.Size = new System.Drawing.Size(571, 109);
            this.dgvDSSBTG.TabIndex = 7;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ma_chuyen_bay";
            this.dataGridViewTextBoxColumn1.FillWeight = 155.33F;
            this.dataGridViewTextBoxColumn1.HeaderText = "Mã chuyến bay";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // TenSanBay
            // 
            this.TenSanBay.DataPropertyName = "ma_san_bay";
            this.TenSanBay.FillWeight = 61.55668F;
            this.TenSanBay.HeaderText = "Mã sân bay";
            this.TenSanBay.Name = "TenSanBay";
            this.TenSanBay.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "thoi_gian_dung";
            this.Column8.FillWeight = 61.55668F;
            this.Column8.HeaderText = "Thời gian dừng";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "ghi_chu";
            this.Column9.FillWeight = 61.55668F;
            this.Column9.HeaderText = "Ghi chú";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            // 
            // btnDel_sbtg
            // 
            this.btnDel_sbtg.Location = new System.Drawing.Point(599, 253);
            this.btnDel_sbtg.Name = "btnDel_sbtg";
            this.btnDel_sbtg.Size = new System.Drawing.Size(142, 23);
            this.btnDel_sbtg.TabIndex = 44;
            this.btnDel_sbtg.Text = "Xóa sân bay trung gian";
            this.btnDel_sbtg.UseVisualStyleBackColor = true;
            // 
            // btnEdit_sbtg
            // 
            this.btnEdit_sbtg.Location = new System.Drawing.Point(767, 253);
            this.btnEdit_sbtg.Name = "btnEdit_sbtg";
            this.btnEdit_sbtg.Size = new System.Drawing.Size(142, 23);
            this.btnEdit_sbtg.TabIndex = 45;
            this.btnEdit_sbtg.Text = "Sửa sân bay trung gian";
            this.btnEdit_sbtg.UseVisualStyleBackColor = true;
            // 
            // frmChuyenBay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 517);
            this.Controls.Add(this.btnEdit_sbtg);
            this.Controls.Add(this.btnDel_sbtg);
            this.Controls.Add(this.btnAdd_sbtg);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtMaCB);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtGC);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtTGD);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.cbSBTG);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.dtpNgayGio);
            this.Controls.Add(this.cbSBDen);
            this.Controls.Add(this.cbSBDi);
            this.Controls.Add(this.txtslGhedadathang2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtslGhedadathang1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtSlghehang2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtSlghehang1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtThoiGianBay);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Name = "frmChuyenBay";
            this.Text = "Nhận lịch chuyến bay";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDSCB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDSSBTG)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtThoiGianBay;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSlghehang1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSlghehang2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtslGhedadathang2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtslGhedadathang1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbSBDi;
        private System.Windows.Forms.ComboBox cbSBDen;
        private System.Windows.Forms.DateTimePicker dtpNgayGio;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.ComboBox cbSBTG;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTGD;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtGC;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtMaCB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnAdd_sbtg;
        private System.Windows.Forms.DataGridView dgvDSCB;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaSanBay;
        private System.Windows.Forms.DataGridViewTextBoxColumn sanbaydi;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridView dgvDSSBTG;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenSanBay;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.Button btnDel_sbtg;
        private System.Windows.Forms.Button btnEdit_sbtg;
    }
}