﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using dack.BUS;
using dack.DTO;
using System.Data.SqlClient;

namespace dack.GUI
{
    public partial class frmChuyenBay : Form
    {
        public frmChuyenBay()
        {
            InitializeComponent();

            LoadSBdi();
            LoadSBden();
            LoadSBtg();
            LoadDGVcb();
            LoadDGVDSSBTG();

            // CB
            btnAdd.Click += new EventHandler(btnAdd_Click);
            btnDel.Click += new EventHandler(btnDel_Click);
            btnEdit.Click += new EventHandler(btnEdit_Click);
            dgvDSCB.SelectionChanged += new EventHandler(dgvDSCB_SelectionChanged);

            // SBTG
            btnAdd_sbtg.Click += new EventHandler(btnAdd_sbtg_Click);
            btnDel_sbtg.Click += new EventHandler(btnDel_sbtg_Click);
            btnEdit_sbtg.Click += new EventHandler(btnEdit_sbtg_Click);
            dgvDSSBTG.SelectionChanged += new EventHandler(dgvDSSBTG_SelectionChanged);
        }

        void btnEdit_Click(object sender, EventArgs e)
        {
            if (dgvDSCB.SelectedRows.Count > 0)
            {
                int macb = int.Parse(txtMaCB.Text);

                ChuyenBayDTO cb = new ChuyenBayDTO()
                {
                    ma_chuyen_bay = macb,
                    san_bay_di = (cbSBDi.SelectedItem as dynamic).Value,
                    san_bay_den = (cbSBDen.SelectedItem as dynamic).Value,
                    ngay_gio = DateTime.Parse(dtpNgayGio.Text),
                    thoi_gian_bay = int.Parse(txtThoiGianBay.Text),
                    so_ghe_hang1 = int.Parse(txtSlghehang1.Text),
                    so_ghe_hang2 = int.Parse(txtSlghehang2.Text),
                };

                DbAck ack = ChuyenBayBUS.Update(cb);
                if (ack == DbAck.Ok)
                {
                    MessageBox.Show("Sửa thành công");
                    LoadDGVcb();
                }
                else
                {
                    MessageBox.Show("Có lỗi xảy ra");
                }
            }
        }

        void btnDel_Click(object sender, EventArgs e)
        {
            if (dgvDSCB.SelectedRows.Count > 0) { 
                int macb = int.Parse(dgvDSCB.SelectedRows[0].Cells[0].Value.ToString());

                ChuyenBayDTO chuyenbay = new ChuyenBayDTO() { 
                    ma_chuyen_bay = macb,
                };

                DbAck ack = ChuyenBayBUS.Delete(chuyenbay);
                if(ack == DbAck.Ok){
                    MessageBox.Show("Xóa thành công");
                    LoadDGVcb();
                }else{
                    MessageBox.Show("Có lỗi xảy ra");
                }
            }
        }

        void btnEdit_sbtg_Click(object sender, EventArgs e)
        {
            if (dgvDSCB.SelectedRows.Count > 0 && dgvDSSBTG.SelectedRows.Count > 0)
            {
                int macb = int.Parse(txtMaCB.Text);
                int masb = int.Parse(dgvDSSBTG.SelectedRows[0].Cells[0].Value.ToString());
                int tgd = int.Parse(txtTGD.Text);
                string gc = txtGC.Text;

                SanBayTrungGianDTO sb = new SanBayTrungGianDTO()
                {
                    ma_san_bay = masb,
                    ma_chuyen_bay = macb,
                    thoi_gian_dung = tgd,
                    ghi_chu = gc,
                };

                DbAck ack = SanBayTrungGianBUS.Update(sb);
                if (ack == DbAck.Ok)
                {
                    MessageBox.Show("Sửa thành công");
                    LoadDGVDSSBTG();
                }
                else
                {
                    MessageBox.Show("Có lỗi xảy ra.");
                }
            }
        }

        void dgvDSSBTG_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvDSSBTG.SelectedRows.Count > 0) {
                SanBayDTO sb = SanBayBUS.GetSanBayByID(int.Parse(dgvDSSBTG.SelectedRows[0].Cells[0].Value.ToString()));
                cbSBTG.SelectedIndex = cbSBTG.FindStringExact(sb.ten_san_bay.ToString());
                //cbSBTG.SelectedIndex = int.Parse(dgvDSSBTG.SelectedRows[0].Cells[0].Value.ToString());

                txtTGD.Text = dgvDSSBTG.SelectedRows[0].Cells[2].Value.ToString();
                txtGC.Text = dgvDSSBTG.SelectedRows[0].Cells[3].Value.ToString();
            }
        }

        void btnDel_sbtg_Click(object sender, EventArgs e)
        {
            if (dgvDSCB.SelectedRows.Count > 0 && dgvDSSBTG.SelectedRows.Count > 0) {
                int macb = int.Parse(txtMaCB.Text);
                int masb = int.Parse(dgvDSSBTG.SelectedRows[0].Cells[0].Value.ToString());

                SanBayTrungGianDTO sb = new SanBayTrungGianDTO()
                { 
                    ma_san_bay = masb,
                    ma_chuyen_bay = macb,
                };

                DbAck ack = SanBayTrungGianBUS.Delete(sb);
                if (ack == DbAck.Ok)
                {
                    MessageBox.Show("Xóa thành công");
                    LoadDGVDSSBTG();
                }
                else {
                    MessageBox.Show("Có lỗi xảy ra.");
                }
            }
        }

        void dgvDSCB_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvDSCB.SelectedRows.Count > 0) {
                LoadDGVDSSBTG();
                txtMaCB.Text = dgvDSCB.SelectedRows[0].Cells[0].Value.ToString();

                SanBayDTO sb = SanBayBUS.GetSanBayByID(int.Parse(dgvDSCB.SelectedRows[0].Cells[1].Value.ToString()));
                cbSBDi.SelectedIndex = cbSBDi.FindStringExact(sb.ten_san_bay);

                sb = SanBayBUS.GetSanBayByID(int.Parse(dgvDSCB.SelectedRows[0].Cells[2].Value.ToString()));
                cbSBDen.SelectedIndex = cbSBDen.FindStringExact(sb.ten_san_bay);

                dtpNgayGio.Value = DateTime.Parse(dgvDSCB.SelectedRows[0].Cells[3].Value.ToString());
                txtThoiGianBay.Text = dgvDSCB.SelectedRows[0].Cells[4].Value.ToString();
                txtSlghehang1.Text = dgvDSCB.SelectedRows[0].Cells[5].Value.ToString();
                txtSlghehang2.Text = dgvDSCB.SelectedRows[0].Cells[6].Value.ToString();
                txtslGhedadathang1.Text = dgvDSCB.SelectedRows[0].Cells[7].Value.ToString();
                txtslGhedadathang2.Text = dgvDSCB.SelectedRows[0].Cells[8].Value.ToString();
            }
        }

        void btnAdd_sbtg_Click(object sender, EventArgs e)
        {
            int n;
            if (cbSBTG.SelectedIndex == -1) {
                MessageBox.Show("Chọn sân bay trung gian");
            }else if(txtTGD.Text == ""){
                MessageBox.Show("Nhập thời gian dừng");
            }
            else if (txtTGD.Text != "" && int.TryParse(txtTGD.Text, out n) == false)
            {
                MessageBox.Show("Nhập thời gian dừng bằng số.");
            }
            else if (txtGC.Text == "")
            {
                MessageBox.Show("Nhập ghi chú.");
            }
            else {
                if (dgvDSCB.SelectedRows.Count > 0) {
                    int maCB = int.Parse(dgvDSCB.SelectedRows[0].Cells[0].Value.ToString());
                    SanBayTrungGianDTO sbtg = new SanBayTrungGianDTO() { 
                        ma_chuyen_bay = maCB,
                        ma_san_bay = (cbSBTG.SelectedItem as dynamic).Value,
                        thoi_gian_dung = int.Parse(txtTGD.Text),
                        ghi_chu = txtGC.Text,
                    };

                    DbAck ack = SanBayTrungGianBUS.Insert(sbtg);
                    if (ack == DbAck.Ok)
                    {
                        MessageBox.Show("Thêm thành công");
                        LoadDGVDSSBTG();
                    }
                    else if (ack == DbAck.Fullsbtg) {
                        MessageBox.Show("Số lượng sân bay trung gian quá quy định.");
                    }
                    else if (ack == DbAck.tgdtd)
                    {
                        MessageBox.Show("Thời gian dừng quá thời gian dừng tối đa.");
                    }
                    else if (ack == DbAck.tgdtt)
                    {
                        MessageBox.Show("Thời gian dừng quá thời gian dừng tối thiểu.");
                    }
                    else
                    {
                        MessageBox.Show("Có lỗi xảy ra");
                    }
                }
            }
        }

        void LoadDGVDSSBTG() {
            if (dgvDSCB.SelectedRows.Count > 0)
            {
                int maCB = int.Parse(dgvDSCB.SelectedRows[0].Cells[0].Value.ToString());
                List<SanBayTrungGianDTO> lstSBTG = SanBayTrungGianBUS.GetSanBayTrungGianByChuyenBayID(maCB);
                dgvDSSBTG.DataSource = lstSBTG;
            }
        }

        void btnAdd_Click(object sender, EventArgs e)
        {
            int n;
            if (cbSBDi.SelectedIndex == -1) {
                MessageBox.Show("Chọn sân bay đi");
            }
            else if (cbSBDen.SelectedIndex == -1) {
                MessageBox.Show("Chọn sân bay đến.");
            }
            else if (txtThoiGianBay.Text == "") {
                MessageBox.Show("Nhập thời gian bay");
            }
            else if (txtThoiGianBay.Text != "" && int.TryParse(txtThoiGianBay.Text, out n) == false)
            {
                MessageBox.Show("Nhập thời gian bay bằng số.");
            }
            else if (txtSlghehang1.Text == "")
            {
                MessageBox.Show("Nhập số lượng ghế hạng 1.");
            }
            else if (txtSlghehang1.Text != "" && int.TryParse(txtSlghehang1.Text, out n) == false)
            {
                MessageBox.Show("Nhập số lượng ghế là số.");
            }
            else if (txtSlghehang2.Text == "")
            {
                MessageBox.Show("Nhập số lượng ghế hạng 2.");
            }
            else if (txtSlghehang2.Text != "" && int.TryParse(txtSlghehang2.Text, out n) == false)
            {
                MessageBox.Show("Nhập số lượng ghế là số.");
            }
            else
            {
                ChuyenBayDTO cb = new ChuyenBayDTO()
                {
                    san_bay_di = (cbSBDi.SelectedItem as dynamic).Value,
                    san_bay_den = (cbSBDen.SelectedItem as dynamic).Value,
                    ngay_gio = DateTime.Parse(dtpNgayGio.Text),
                    thoi_gian_bay = int.Parse(txtThoiGianBay.Text),
                    so_ghe_hang1 = int.Parse(txtSlghehang1.Text),
                    so_ghe_hang2 = int.Parse(txtSlghehang2.Text),
                };

                DbAck ack = ChuyenBayBUS.Insert(cb);
                if (ack == DbAck.Ok)
                {
                    MessageBox.Show("Thêm thành công.");
                    LoadDGVcb();
                }
                else if (ack == DbAck.Fulltgbtt)
                {
                    MessageBox.Show("Thời gian bay quá quy định.");
                    LoadDGVcb();
                }
                else {
                    MessageBox.Show("Có lỗi xảy ra.");
                }
            }
        }

        public void LoadDGVcb() {
            List<ChuyenBayDTO> lstCB = ChuyenBayBUS.SelectAllChuyenBay();
            dgvDSCB.DataSource = lstCB;
        }

        public void LoadSBdi() {
            cbSBDi.Items.Clear();
            List<SanBayDTO> lstSB = SanBayBUS.SelectAllSanBay();

            cbSBDi.DisplayMember = "Text";
            cbSBDi.ValueMember = "Value";
            if (lstSB != null) {
                foreach (var item in lstSB) {
                    cbSBDi.Items.Add(new { Value = item.ma_san_bay, Text = item.ten_san_bay});
                }
            }
        }

        public void LoadSBden() {
            cbSBDen.Items.Clear();
            List<SanBayDTO> lstSB = SanBayBUS.SelectAllSanBay();

            cbSBDen.DisplayMember = "Text";
            cbSBDen.ValueMember = "Value";
            if (lstSB != null)
            {
                foreach (var item in lstSB)
                {
                    cbSBDen.Items.Add(new { Value = item.ma_san_bay, Text = item.ten_san_bay });
                }
            }
        }

        public void LoadSBtg() {
            cbSBTG.Items.Clear();
            List<SanBayDTO> lstSB = SanBayBUS.SelectAllSanBay();

            cbSBTG.DisplayMember = "Text";
            cbSBTG.ValueMember = "Value";
            if (lstSB != null)
            {
                foreach (var item in lstSB)
                {
                    cbSBTG.Items.Add(new { Value = item.ma_san_bay, Text = item.ten_san_bay });
                }
            }
        }
    }
}
