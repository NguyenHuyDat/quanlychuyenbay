﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DAO;
using dack.DTO;
using dack.BUS;

namespace dack.BUS
{
    class ThamSoBUS
    {
        public static ThamSoDTO SelectAllThamSo()
        {
            ThamSoDTO thamso = null;

            thamso = ThamSoDAO.SelectAllThamSo();

            return thamso;
        }

        public static ThamSoDTO GetThamSoByID(int ThamSoID)
        {
            ThamSoDTO thamso = null;

            thamso = ThamSoDAO.GetThamSoByID(ThamSoID);

            return thamso;
        }

        // 
        public static DbAck Update(ThamSoDTO thamSo)
        {
            DbAck result = DbAck.Unknow;

            result = ThamSoDAO.Update(thamSo);

            return DbAck.Ok;
        }
    }
}
