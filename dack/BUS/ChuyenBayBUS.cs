﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DAO;
using dack.DTO;

namespace dack.BUS
{
    public class ChuyenBayBUS
    {
        public static List<ChuyenBayDTO> SelectAllChuyenBay()
        {
            List<ChuyenBayDTO> list = ChuyenBayDAO.SelectAllChuyenBay();
            return list;
        }

        public static ChuyenBayDTO GetChuyenBayByID(int id)
        {
            ChuyenBayDTO cb = ChuyenBayDAO.GetChuyenBayByID(id);
            return cb;
        }

        public static DbAck Insert(ChuyenBayDTO chuyenbay)
        {
            DbAck result = DbAck.Unknow;

            ThamSoDTO ts = ThamSoDAO.SelectAllThamSo();
            if (chuyenbay.thoi_gian_bay > ts.thoi_gian_bay_toi_thieu) {
                return DbAck.Fulltgbtt;
            }

            result = ChuyenBayDAO.Insert(chuyenbay);
            return DbAck.Ok;
        }

        public static DbAck Update(ChuyenBayDTO cb)
        {
            DbAck result = DbAck.Unknow;

            result = ChuyenBayDAO.Update(cb);

            return result;
        }

        public static DbAck Delete(ChuyenBayDTO cb)
        {
            DbAck result = DbAck.Unknow;

            result = ChuyenBayDAO.Delete(cb);

            return result;
        }

        public static ChuyenBayDTO getIDMax() {
            ChuyenBayDTO cb = ChuyenBayDAO.getIDMax();
            return cb;
        }
        
    }
}
