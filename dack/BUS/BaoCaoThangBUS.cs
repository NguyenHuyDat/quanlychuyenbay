﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DAO;
using dack.DTO;

namespace dack.BUS
{
    public class BaoCaoThangBUS
    {
        public static List<BaoCaoThangDTO> GetData()
        {
            List<BaoCaoThangDTO> list = BaoCaoThangDAO.GetData();
            return list;
        }
        public static List<BaoCaoThangDTO> FindItem(int id)
        {
            List<BaoCaoThangDTO> list = BaoCaoThangDAO.FindItem(id);
            return list;
        }
    }
}
