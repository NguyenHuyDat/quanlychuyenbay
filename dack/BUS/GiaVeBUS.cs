﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using dack.DAO;

namespace dack.BUS
{
    public class GiaVeBUS
    {
        public static List<GiaVeDTO> SelectAllGiaVe()
        {
            List<GiaVeDTO> ListGiaVe = null;

            ListGiaVe = GiaVeDAO.SelectAllGiaVe();

            return ListGiaVe;
        }

        public static GiaVeDTO GetGiaVeByID(int masb, int mahv)
        {
            GiaVeDTO GiaVe = null;

            GiaVe = GiaVeDAO.GetGiaVeByID(masb, mahv);

            return GiaVe;
        }

        public static DbAck Insert(GiaVeDTO giave)
        {
            DbAck result = DbAck.Unknow;

            result = GiaVeDAO.Insert(giave);

            return DbAck.Ok;
        }

        public static DbAck Update(GiaVeDTO giave)
        {
            DbAck result = DbAck.Unknow;

            result = GiaVeDAO.Update(giave);

            return result;
        }

        public static DbAck Delete(GiaVeDTO giave)
        {
            DbAck result = DbAck.Unknow;
            result = GiaVeDAO.Delete(giave);
            return result;
        }
    }
}
