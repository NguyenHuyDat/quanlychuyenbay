﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DAO;
using dack.DTO;
using dack.BUS;

namespace dack.BUS
{
    public class SanBayBUS
    {
        public static List<SanBayDTO> SelectAllSanBay()
        {
            List<SanBayDTO> listSanBay = new List<SanBayDTO>();

            listSanBay = SanBayDAO.SelectAllSanBay();

            return listSanBay;
        }

        public static SanBayDTO GetSanBayByID(int SanBayID)
        {
            SanBayDTO sanbay = new SanBayDTO();

            sanbay = SanBayDAO.GetSanBayByID(SanBayID);

            return sanbay;
        }

        public static DbAck Insert(SanBayDTO sanbay)
        {
            DbAck result = DbAck.Unknow;

            result = SanBayDAO.Insert(sanbay);

            return DbAck.Ok;
        }

        public static DbAck Update(SanBayDTO sanbay)
        {
            DbAck result = DbAck.Unknow;

            result = SanBayDAO.Update(sanbay);

            return result;
        }

        public static DbAck Delete(SanBayDTO sanbay)
        {
            DbAck result = DbAck.Unknow;

            result = SanBayDAO.Delete(sanbay);

            return result;
        }
    }
}
