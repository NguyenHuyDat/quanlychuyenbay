﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using dack.DAO;

namespace dack.BUS
{
    public class SanBayTrungGianBUS
    {
        public static DbAck Insert(SanBayTrungGianDTO sanbay)
        {
            DbAck result = DbAck.Unknow;

            ThamSoDTO ts = ThamSoDAO.SelectAllThamSo();

            List<SanBayTrungGianDTO> Lsbtg = SanBayTrungGianDAO.GetSanBayTrungGianByChuyenBayID(sanbay.ma_chuyen_bay);
            if (Lsbtg != null)
            {
                if (Lsbtg.Count >= ts.so_luong_san_bay_trung_gian_toi_da)
                {
                    return DbAck.Fullsbtg;
                }
            }

            if (sanbay.thoi_gian_dung >= ts.thoi_gian_dung_toi_da)
                return DbAck.tgdtd;

            if (sanbay.thoi_gian_dung <= ts.thoi_gian_dung_toi_thieu)
                return DbAck.tgdtt;

            result = SanBayTrungGianDAO.Insert(sanbay);
            return DbAck.Ok;
        }

        public static List<SanBayTrungGianDTO> GetSanBayTrungGianByChuyenBayID(int id)
        {
            List<SanBayTrungGianDTO> l = SanBayTrungGianDAO.GetSanBayTrungGianByChuyenBayID(id);
            return l;
        }

        public static DbAck Delete(SanBayTrungGianDTO sb)
        {
            DbAck rs = DbAck.Unknow;

            rs = SanBayTrungGianDAO.Delete(sb);

            return rs;
        }

        public static DbAck Update(SanBayTrungGianDTO sb)
        {
            DbAck rs = DbAck.Unknow;

            rs = SanBayTrungGianDAO.Update(sb);

            return rs;
        }
    }
}