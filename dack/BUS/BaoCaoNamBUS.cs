﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using dack.DAO;

namespace dack.BUS
{
    public class BaoCaoNamBUS
    {
        public static List<BaoCaoNamDTO> GetData()
        {
            List<BaoCaoNamDTO> list = BaoCaoNamDAO.GetData();
            return list;
        }
        public static List<BaoCaoNamDTO> FindItem(int id)
        {
            List<BaoCaoNamDTO> list = BaoCaoNamDAO.FindItem(id);
            return list;
        }
    }
}
