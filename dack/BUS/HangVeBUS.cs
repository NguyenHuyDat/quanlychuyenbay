﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using dack.DAO;

namespace dack.BUS
{
    public class HangVeBUS
    {
        public static List<HangVeDTO> SelectAllHangVe()
        {
            List<HangVeDTO> listHangVe = null;

            listHangVe = HangVeDAO.SelectAllHangVe();

            return listHangVe;
        }

        public static HangVeDTO GethangVeByID(int hangVeID)
        {
            HangVeDTO hangVe = null;
            hangVe = HangVeDAO.GethangVeByID(hangVeID);
            return hangVe;
        }

        public static DbAck Insert(HangVeDTO hangve)
        {
            DbAck result = DbAck.Unknow;
            result = HangVeDAO.Insert(hangve);
                
            return result;
        }

        public static DbAck Update(HangVeDTO hangve)
        {
            DbAck result = DbAck.Unknow;
            result = HangVeDAO.Update(hangve);
            return result;
        }

        public static DbAck Delete(HangVeDTO hangve)
        {
            DbAck result = DbAck.Unknow;
            result = HangVeDAO.Delete(hangve);
            return result;
        }
    }
}
