﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DTO;
using dack.DAO;

namespace dack.BUS
{
    public class VeChuyenBayBUS
    {
        public static List<VeChuyenBayDTO> SelectAllVeChuyenBay()
        {
            List<VeChuyenBayDTO> listVeChuyenBay = null;

            listVeChuyenBay = VeChuyenBayDAO.SelectAllVeChuyenBay();

            return listVeChuyenBay;
        }

        public static VeChuyenBayDTO GetVeChuyenBayByID(int vcbID)
        {
            VeChuyenBayDTO vcb = null;

            vcb = VeChuyenBayDAO.GetChuyenBayByID(vcbID);

            return vcb;
        }

        public static DbAck Insert(VeChuyenBayDTO vcb)
        {
            DbAck result = DbAck.Unknow;

            result = VeChuyenBayDAO.Insert(vcb);

            return result;
        }

        public static DbAck Update(VeChuyenBayDTO vcb)
        {
            DbAck result = DbAck.Unknow;

            result = VeChuyenBayDAO.Update(vcb);

            return result;
        }

        public static DbAck Delete(VeChuyenBayDTO vcb)
        {
            DbAck result = DbAck.Unknow;

            result = VeChuyenBayDAO.Delete(vcb);

            return result;
        }
    }
}
