﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using dack.DAO;
using dack.DTO;
using dack.BUS;

namespace dack.BUS
{
    class HanhKhachBUS
    {
        public static List<HanhKhachDTO> SelectAllHanhKhach()
        {
            List<HanhKhachDTO> listHanhKhach = new List<HanhKhachDTO>();

            listHanhKhach = HanhKhachDAO.SelectAllHanhKhach();

            return listHanhKhach;
        }

        public static HanhKhachDTO GetHanhKhachByID(int HanhKhachID)
        {
            HanhKhachDTO HanhKhach = null;

            return HanhKhach;
        }

        public static DbAck Insert(HanhKhachDTO HanhKhach)
        {
            DbAck result = DbAck.Unknow;

            result = HanhKhachDAO.Insert(HanhKhach);

            return DbAck.Ok;
        }

        public static DbAck Update(HanhKhachDTO HanhKhach)
        {
            DbAck result = DbAck.Unknow;
            result = HanhKhachDAO.Update(HanhKhach);
            return result;
        }

        public static DbAck Delete(HanhKhachDTO HanhKhach)
        {
            DbAck result = DbAck.Unknow;
            result = HanhKhachDAO.Delete(HanhKhach);
            return result;
        }
    }
}
